(ns mulify.api-gateway
  (:require [mulify.core :refer (defprocess global-process-map >camel)]))

(defprocess autodiscovery {:api-id "" :ignore-base-path "true" :flow-ref ""} :key-fn >camel)

