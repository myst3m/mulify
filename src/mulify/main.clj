(ns mulify.main
  (:gen-class )
  (:require [clojure.tools.cli :refer (parse-opts)]
            [clojure.string :as str]
            [clojure.java.io :as io]
            [clojure.xml :as xml]
            [clojure.zip :as zip]
            [clojure.data.xml :as dx]
            [silvur.datetime]
            [silvur.util :refer [uuid]]
            [taoensso.timbre :as log]))


(def cli-options [["-h" "--help" "This Help"]
                  ["-l" "--level <LEVEL>" "Depth level"
                   :parse-fn #(Integer/parseInt %)]])


(defn usage [summary]
  (println)
  (println "Usage:")
  (println)
  (println summary)
  (println))



;; (defn iter-zip [zipper]
;;   (->> zipper
;;        (iterate zip/next)
;;        (take-while (complement zip/end?))))

;; (defonce xz  (zip/xml-zip (xml/parse (io/input-stream "example.xml"))))
;; (def x (xml/parse (io/input-stream "example.xml")))

(defn xml->hiccup [tree & [opts]]
  (let [xml-tree (if (or (string? tree) (instance? java.net.URL tree))
                   (xml/parse (io/input-stream tree))
                   tree)]
    ((fn impl [a-tree {:keys [level attrs?]
                       :or {attrs? true level Integer/MAX_VALUE}
                       :as a-opts}]
       (when (< 0 (inc level))
         (cond
           (and (map? a-tree) (:tag a-tree)) (cond-> [(:tag a-tree)]
                                               (and attrs? (:attrs a-tree)) (conj (:attrs a-tree))
                                               (:content a-tree) (as-> m
                                                                     (apply conj m (impl (:content a-tree)
                                                                                         {:level (dec level)
                                                                                          :attrs? attrs?}))))
           (and (sequential? a-tree)) (filter (complement empty?) (map #(impl % a-opts) a-tree))
           (string? a-tree) (str/trim a-tree)
           :else a-tree)))
     xml-tree opts)))


(defn zip-item 
  ([tag]
   (zip-item tag [] {}))
  ([tag content & [attrs]]
   (conj {:tag tag
          :content content}
         (when attrs {:attrs attrs}))))

(defn hiccup->zip [root]
  (let [root-tag (first root)
        zip-root (zip-item root-tag)]
    (zip/xml-zip
     ((fn impl [r]
        (cond
          (string? (last r)) (zip-item (first r) [(last r)])
          (vector? (second r)) (zip-item (first r) (mapv impl (rest r)))
         
          (map? (second r)) (zip-item (first r) (mapv impl (drop 2 r)) (second r) )
         
          (nil? (second r)) (zip-item (first r))
          :else (throw (ex-info "No impl" {:r r}))))
      root))))

(defn xml-query [ks v & [xsd]]
  (let [xsd (or xsd "http://www.mulesoft.org/schema/mule/core/current/mule.xsd")]
    (->> (xml-seq (xml/parse (io/input-stream xsd)))
         (filter #(= v (get-in % (vec (flatten [ks])))) )
         (map xml->hiccup ))))

(defmacro choice [exp if-body else-body]
  `[:choice
    {:doc:id (uuid)}
    [:when
     {:expression ~exp}
     ~if-body]
    [:otherwise
     ~else-body]]
  )

(defn -main [& args]
  (log/set-level! :error)
  (let [{:keys [options arguments summary errors]} (parse-opts
                                                    args
                                                    cli-options)]
    (if (or (:help options) (empty? arguments))
      (usage summary)
      (clojure.pprint/pprint (xml->hiccup (xml/parse (io/input-stream (first arguments))) (or (:level options) 2))))))


(comment
  (second (xml-query :tag :choice "example.xml"))
  
  [:choice
   {:doc:id "d52b4fee-9835-4f3d-979d-dbcab1814940", :doc:name "Choice"}
   [:when
    {:expression "#[payload.handler.name == \"order\"]"}
    [:ee:transform
     {:doc:id "59c3024c-9a98-469e-8900-8b76bd352352",
      :doc:name "Transform Message"}
     [:ee:message
      [:ee:set-payload
       "%dw 2.0\n            output application/json\n            ---\n            {\n\t    productCode: payload.scene.slots.productCode.value,\n\t    size: payload.scene.slots.size.value,\n\t    amount: payload.scene.slots.amount.value,\n            email: \"myst3m@gmail.com\",\n            name: \"Miyashita\",\n            address1: \"Tokyo\",\n            address2: \"Apartment 502\",\n            city: \"Tokyo\",\n            stateOfProvince: \"Suginami-ku\",\n            postalCode: \"166-0001\",\n            country: \"Japan\"\n\t    \n            }"]]]
    [:http:request
     {:path "/api/order",
      :config-ref "HTTP_Request_localhost_api",
      :doc:id "4e6639d6-f3c7-4276-b7a0-3adde35c0f1a",
      :doc:name "Request",
      :method "POST"}]
    [:logger
     {:message "Order RESPONSE: #[payload]",
      :doc:id "ded4ba4b-65cd-4ad8-8b86-5e185317b772",
      :doc:name "Logger",
      :level "INFO"}]
    [:ee:transform
     {:doc:id "9a2be4ff-3180-4f72-acf0-42073cc62ad4",
      :doc:name "Transform Message"}
     [:ee:message
      [:ee:set-payload
       "%dw 2.0\n            output application/json\n            ---\n            {\"prompt\":\n\t    {\"firstSimple\":\n\t    {\"speech\": \"受付IDは\" ++ payload.orderId as String ++ \"です\",\n\t    \"text\":\"\"},\n\t    \"override\":false},\n\t    \"scene\":{\"slots\":{},\"name\":\"name\"},\n\t    \"session\":{\"params\":{},\"id\": vars.sessionId}\n\t    }"]]]
    [:logger
     {:message "Response: #[payload]",
      :doc:id "498288c4-ca91-4e76-ab20-b8137991038f",
      :doc:name "Logger",
      :level "INFO"}]]
   [:otherwise
    [:http:request
     {:path "/api/inventory",
      :config-ref "HTTP_Request_localhost_api",
      :doc:id "da3c21d6-7244-428b-87c9-ad7dc87426c0",
      :doc:name "Request",
      :method "GET"}
     [:http:query-params
      "#[output application/java\n          ---\n          {\n\t  \"productCode\" : payload.scene.slots.productCode.value,\n\t  \"size\": payload.scene.slots.size.value\n          }]"]]
    [:logger
     {:message "#[payload]",
      :doc:id "3956fb42-d6ba-42a6-a3aa-6625a6366d3f",
      :doc:name "Logger",
      :level "INFO"}]
    [:ee:transform
     {:doc:id "651ae6e0-106d-400a-8415-4e3b86d0b863",
      :doc:name "Transform Message"}
     [:ee:message
      [:ee:set-payload
       "%dw 2.0\n            output application/json\n            ---\n            {\"prompt\":{\"firstSimple\":\n\t    {\"speech\": payload[0].productCode ++ \"の\" ++ payload[0].size ++ \"サイズの在庫は、\" ++ payload[0].count ++ \"です\",\n\t    \"text\":\"\"},\n\t    \"override\":false},\n\t    \"scene\":{\"slots\":{},\"name\":\"name\"},\n\t    \"session\":{\"params\":{},\"id\": vars.sessionId}\n\t    }"]]]]])
