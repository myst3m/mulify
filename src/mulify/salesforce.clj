(ns mulify.salesforce
  (:require [clojure.data.xml :as dx]
            [silvur.util :refer [uuid]]
            [mulify.core :refer (defprocess >camel ) :as c]
            [clojure.spec.alpha :as s]
            [clojure.spec.test.alpha :as t]
            [mulify.dataweave :as dw]))

(defn string->cdata [x]
  (if (string? x) (dx/cdata x) x))

(defprocess sfdc-config {:name "Salesforce_Config"})
(defprocess basic-connection {:username "" :password "" :security-token ""})

;; Query is Deprecated, use Query XML Stream
(defprocess query {:config-ref "Salesforce_Config"}
  :update-fn (fn [x] x))

(defprocess query-xml-stream {:config-ref "Salesforce_Config"}
  :update-fn (fn [x] x))


(defprocess salesforce-query {} :update-fn string->cdata)

(defprocess create-job-bulk-api-v2 {:external-id-field-name "Id"
                                    :config-ref "salesforce-config"
                                    :operation "upsert"
                                    :object-type "Account"})
(defprocess delete-job-bulk-api-v2 {:config-ref "salesforce-config"})
(defprocess get-job-state-bulk-api-v2 {:config-ref "salesforce-config"})
(defprocess get-all-jobs-bulk-api-v2 {:config-ref "salesforce-config"})

(defprocess parameters {} :update-fn string->cdata)
(defprocess replay-channel-listener)

(defprocess config-with-oauth-connection)
(defprocess oauth-authorization-code {:consumer-key "" :consumer-secret ""})
(defprocess oauth-user-pass-connection {:consumer-key "" :consumer-secret ""})
(defprocess auth-callback-config {:listener-config "" :callback-path "" :authorize-path ""})


(defn sfdc-config* [{:keys [name username password security-token] :as args}]
  (sfdc-config {:name name}
    (basic-connection (dissoc args :name))))

(defn- get-config-name [n]
  (-> c/global-configs
      (->> (filter #(and (#{'mulify.salesforce :salesforce 'sf} (:class %)) (= (keyword (:id %)) (keyword n))) ))
      first 
      :id
      (as-> id
          (or id (throw (ex-info "Not found config for salesforce in global-configuration"
                                 {:given-id n}))))
      (->> (str "salesforce-config-"))))

(defmulti config* (fn [{:keys [id username password security-token type]}] type))
(defmethod config* :default [{:keys [id username password security-token type] :as args}]
  (sfdc-config {:name (get-config-name id)}
               (basic-connection (select-keys args [:username :password :security-token]))))


(defn query-classic* [sf-config query-body & params]
  ;; Assume sf-config is XML tree
  (query {:config-ref (if (map? sf-config)
                        (get-in sf-config [:attrs :name])
                        (get-config-name sf-config))}
    (salesforce-query (dx/cdata query-body))
    (parameters (dw/fx (into {} (map vec (partition-all 2 params)))))))

(defn query* [sf-config query-body & params]
  ;; Assume sf-config is XML tree
  (query-xml-stream {:config-ref (if (map? sf-config)
                                   (get-in sf-config [:attrs :name])
                                   (get-config-name sf-config))}
    (salesforce-query (dx/cdata query-body))
    (parameters (dw/fx (into {} (map vec (partition-all 2 params)))))))


;; Configuration
(s/fdef sfdc-config
  :args (s/cat
          :attr (s/keys :req-un [::name])
          :auth (s/alt
                 :basic-connection #(-> % :tag #{::basic-connection})
                 :oauth-user-pass-connection #(-> % :tag #{::oauth-user-pass-connection}))))

(s/fdef basic-connection
  :args (s/cat :credential (s/keys :req-un [::username ::password ::security-token])))

(s/fdef oauth-user-pass-connection
  :args (s/cat :credential (s/keys :req-un [::username
                                            ::password
                                            ::consumer-key
                                            ::consumer-secret])))



;; Query
(s/fdef query
  :args (s/cat
          :attr (s/? (s/keys :req-un [::config-ref] :opt-un [::target]))
          :salesforce-query (s/* #(-> % :tag #{::salesforce-query ::parameters}))))

(s/fdef salesforce-query
  :args (s/cat :cdata (s/alt
                        :cdata (partial instance? clojure.data.xml.node.CData)
                        :string string?)))

(s/fdef parameters
  :args (s/alt
          :cdata (partial instance? clojure.data.xml.node.CData)
          :string string?))


(s/fdef config-with-oauth-connection
  :args (s/cat
          :oauth-authorization-code #(-> % :tag #{::oauth-authorization-code})
          :auth-callback-config #(-> % :tag #{::auth-callback-config})))

(s/fdef oauth-authorization-code
  :args (s/cat 
          :attr (s/keys :req-un [::consumer-key ::consumer-secret])))

(s/fdef auth-callback-config
  :args (s/cat 
          :attr (s/keys :req-un [::listener-config ::callback-path ::authorize-path])))


(t/instrument)







