(ns mulify.sftp
  (:require [clojure.data.xml :as dx]
            [silvur.util :refer [uuid]]
            [clojure.string :as str]
            [mulify.core :refer (defprocess >camel)]))

(defprocess listener)

(defprocess config)

(defprocess matcher )

(defprocess connection)

(defprocess preferred-authentication-methods)


