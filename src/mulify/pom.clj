(ns mulify.pom
  (:gen-class)
  (:require
   [clojure.string :as str]
   [silvur.util :refer (json->edn edn->json edn->xml)]
   [silvur.nio :as nio]
   [clojure.java.io :as io]))

(def pom-content
 {:project
  {:properties
   {:project.build.sourceEncoding "UTF-8",
    :mule.maven.plugin.version "4.3.0",
    :app.runtime "4.8.0",
    :project.reporting.outputEncoding "UTF-8"},
   :groupId "my-group",
   :packaging "mule-application",
   :name "hello-world",
   :-xsi:schemaLocation
   "http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd",
   :-xmlns "http://maven.apache.org/POM/4.0.0",
   :repositories
   {:repository
    [{:layout "default",
      :name "Anypoint Exchange",
      :id "anypoint-exchange-v3",
      :url "https://maven.anypoint.mulesoft.com/api/v3/maven"}
     {:layout "default",
      :name "MuleSoft Releases Repository",
      :id "mulesoft-releases",
      :url "https://repository.mulesoft.org/releases/"}]},
   :-xmlns:xsi "http://www.w3.org/2001/XMLSchema-instance",
   :build
   {:plugins
    {:plugin
     [{:groupId "org.apache.maven.plugins",
       :artifactId "maven-clean-plugin",
       :version "3.2.0"}
      {:groupId "org.mule.tools.maven",
       :artifactId "mule-maven-plugin",
       :extensions "true",
       :version "${mule.maven.plugin.version}",
       :configuration {:sharedLibraries [{:sharedLibrary {:groupId "mysql", :artifactId "mysql-connector-java"}}
                                         {:sharedLibrary {:groupId "org.codehaus.groovy", :artifactId "groovy-all"}}]
                       :classifier "mule-application"}}]}},
   :artifactId "hello-world",
   :pluginRepositories
   {:pluginRepository
    {:layout "default",
     :name "MuleSoft Releases Repository",
     :id "mulesoft-releases",
     :snapshots {:enabled "false"},
     :url "https://repository.mulesoft.org/releases/"}},
   :version "1.0.0-SNAPSHOT",
   :dependencies
   {:dependency
    [{:groupId "com.mulesoft.connectors",
      :classifier "mule-plugin",
      :artifactId "mule-salesforce-connector",
      :version "10.22.4"}
     {:groupId "org.mule.modules",
      :classifier "mule-plugin",
      :artifactId "mule-apikit-module",
      :version "1.11.2"}
     {:groupId "org.mule.connectors",
      :classifier "mule-plugin",
      :artifactId "mule-db-connector",
      :version "1.14.13"}
     {:groupId "mysql",
      :artifactId "mysql-connector-java",
      :version "8.0.30"}
     {:groupId "org.mule.connectors",
      :classifier "mule-plugin",
      :artifactId "mule-http-connector",
      :version "1.10.3"}
     {:groupId "org.mule.modules",
      :classifier "mule-plugin",
      :artifactId "mule-validation-module",
      :version "2.0.6"}
     {:groupId "org.mule.connectors",
      :classifier "mule-plugin",
      :artifactId "mule-sockets-connector",
      :version "1.2.5"}
     {:groupId "org.mule.modules",
      :classifier "mule-plugin",
      :artifactId "mule-scripting-module",
      :version "2.1.1"}
     {:groupId "org.mule.connectors",
      :classifier "mule-plugin",
      :artifactId "mule-file-connector",
      :version "1.5.2"}
     {:groupId "org.codehaus.groovy",
      :classifier "indy",
      :artifactId "groovy-all",
      :version "2.4.21"}
     ]},
   :modelVersion "4.0.0"}})

(defn gen-pom! [group artifact version & {:keys [raw?]}]
  (cond-> pom-content
    :run (assoc-in [:project :groupId] (name group))
    :run (assoc-in [:project :name] (name artifact))
    :run  (assoc-in [:project :artifactId] (name artifact))
    :run (assoc-in [:project :version] version)
    (not raw?) (edn->xml)
    (not raw?) (->> (spit "pom.xml"))))

(defn find-pom-root [dir]
  (loop [d dir]
    (when-not (= d (System/getenv "HOME"))
      (if-let [f (first (filter #(re-find #"pom\.xml" (str %)) (file-seq (io/file d))))]
        d
        (recur "..")))))


(defn pom-update! 
  ([app]
   (let [{:keys [group artifact version]} (meta app)
         artifact (or artifact (str (.getFileName (nio/path (find-pom-root (System/getenv "PWD"))))))]
     (pom-update! group artifact version)
     app))
  ([group artifact version]
   (let [pom-root (find-pom-root (System/getenv "PWD"))
         pom-file-path (io/file pom-root "pom.xml")]
     (nio/copy pom-file-path (str pom-file-path ".bak"))
     (spit
      pom-file-path
      (->
       pom-content
       (assoc-in [:project :groupId] group)
       (assoc-in [:project :name] artifact)
       (assoc-in [:project :artifactId] artifact)
       (assoc-in [:project :version] version)
       (edn->xml))))))

(defn pom-add-dependency [& gav-maps]
  (let [pom-root (find-pom-root (System/getenv "PWD"))
        pom-file-path (io/file pom-root "pom.xml")]
    ;; (nio/copy pom-file-path (str pom-file-path ".bak"))
    (let [pom (->> gav-maps
                (reduce (fn [deps {:keys [group artifact version mule-plugin?]}]
                          (update-in deps [:project :dependencies :dependency]
                                     conj (if mule-plugin?
                                            {:groupId group
                                             :artifactId artifact
                                             :version version
                                             :classifier "mule-plugin"}
                                            {:groupId group
                                             :artifactId artifact
                                             :version version})))
                        pom-content))
          plugins (get-in pom [:project :build :plugins :plugin])
          [maven-plugin] (filter #(= "mule-maven-plugin" (:artifactId %)) plugins)
          others (filter #(not= "mule-maven-plugin" (:artifactId %)) plugins)]
      
      
      (->> (conj others
                 (->> gav-maps
                      (reduce (fn [mp {:keys [group artifact version mule-plugin? with-shared-library?]}]
                                (if with-shared-library?
                                  (update-in mp [:configuration :sharedLibraries]
                                             conj {:sharedLibrary
                                                   {:groupId group :artifactId artifact}})
                                  mp))
                              maven-plugin)))
          (vec)
          (assoc-in pom [:project :build :plugins :plugin])
          (edn->xml)
          (spit pom-file-path)))))


