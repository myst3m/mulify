(ns mulify.tracking
  (:require [mulify.doc :as doc]
            [mulify.dataweave :as dw]
            [silvur.util :refer [uuid]]
            [clojure.data.xml :as dx]))


(defn transaction [{:keys [id]}]
  (dx/element ::transaction {:id id}))

(defn custom-event [{:keys [event-name]} meta-data]
  (dx/element ::custom-event {:doc:name "Custom Business Event"
                              :event-name event-name}
              meta-data))

(defn meta-data [{:keys [key value]}]
  (dx/element ::meta-data {:key key :value value}))

