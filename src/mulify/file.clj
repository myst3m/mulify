(ns mulify.file
  (:refer-clojure :exclude [read])
  (:require [clojure.data.xml :as dx]
            [clojure.string :as str]
            [mulify.core :refer [defprocess]]
            [mulify.dataweave :as dw]
            [silvur.util :refer [uuid edn->json]]))


(defprocess write)
(defprocess read {:output-mime-type "application/json"})
(defprocess matcher)
(defprocess connection)
(defprocess config)
(defprocess listener)
(defprocess content)

