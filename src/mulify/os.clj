(ns mulify.os
  (:refer-clojure :exclude [read])
  (:require [clojure.data.xml :as dx]
            [clojure.string :as str]
            [mulify.core :refer [defprocess]]
            [mulify.dataweave :as dw]
            [mulify.core :refer (defprocess >camel)]))

(defprocess object-store {:name "Object_store" :max-entries "1024" :entry-ttl "1200"})
(defprocess retrieve {:key "os-key" :target "target-column" :target-value "#[payload]"})
(defprocess store {:key "os-name"})
(defprocess default-value)
(defprocess value)


(comment
  (mulify.os/retrieve
   {:key "lastAccountID", :target "lastAccountID"}
   (mulify.os/default-value "0"))
  
  (mulify.os/store
   {:key "lastAccountID"}
   (mulify.os/value "#[max(payload.*accountID)]")))

