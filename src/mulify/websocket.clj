(ns mulify.websocket
  (:refer-clojure :exclude [update send])
  (:require [clojure.data.xml :as dx]
            [silvur.util :refer [uuid]]
            [clojure.string :as str]
            [mulify.core :refer (defprocess >camel)]))

(defprocess config {:name "WebSockets_Config"})
(defprocess connection)
(defprocess server-settings {:listener-config "WS_Listener_config"})
(defprocess client-settings {:port "8089", :protocol "WS", :host "localhost"})

;; Notes the following 2 listeners should be placed together
(defprocess on-inbound-connection {:config-ref "WebSockets_Config" :path "/ws"})
(defprocess inbound-listener {:config-ref "WebSockets_Config" :path "/ws"})

;; Client
(defprocess open-outbound-listener {:config-ref "WebSockets_Config"
                                    :path "/ws"
                                    :socket-id "sock-id" })
(defprocess send {:config-ref "WebSockets_Config" :socket-id "sock-id"})

(comment
  (mulify.core/mule

   (mulify.websocket/config
    {:name "WebSockets_Config"}
    (mulify.websocket/connection

     (mulify.websocket/server-settings
      {:listenerConfig "WS_Listener_config"})

     (mulify.websocket/client-settings
      {:port "8081", :protocol "WS", :host "localhost"})))
   
   (mulify.core/flow
    (mulify.websocket/on-inbound-connection
     {:config-ref "WebSockets_Config", :path "/ws"}))
   
   (mulify.core/flow
    (mulify.websocket/inbound-listener
     {:config-ref "WebSockets_Config", :path "/ws"}))))
