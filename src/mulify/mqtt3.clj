(ns mulify.mqtt3
  (:refer-clojure :exclude [update])
  (:require [clojure.data.xml :as dx]
            [silvur.util :refer [uuid]]
            [clojure.string :as str]
            [mulify.core :refer (defprocess >camel)]))

(defprocess config {:name "MQTT3_Config"})
(defprocess connection {:url "tcp://localhost:1883"})
(defprocess client-id-generator)
(defprocess client-id-random-suffix-generator)

(defprocess listener {:config-ref "MQTT3_Config"})

(defprocess topics)
(defprocess topic {:topic-filter "test/topic"})

(comment
  (mulify.mqtt3/config
  {:name "MQTT3_ConfigX"}
  (mulify.mqtt3/connection
   {:url "tcp://localhost:1883"}
   (mulify.mqtt3/client-id-generator
    (mulify.mqtt3/client-id-random-suffix-generator))))
  
  (mulify.mqtt3/listener
   {:config-ref "MQTT3_ConfigX"}
   (mulify.mqtt3/topics
    (mulify.mqtt3/topic {:topicFilter "test/topic"})))
  )
