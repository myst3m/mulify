(ns mulify.apikit
  (:require [clojure.data.xml :as dx]
            [clojure.string :as str]
            [clojure.java.io :as io]
            [clojure.spec.alpha :as s]
            [clojure.spec.test.alpha :as t]
            [mulify.ee]
            [mulify.http]
            [mulify.dataweave]
            [mulify.core :refer (defprocess global-process-map >camel contains-tag?)]
            [mulify.dataweave :as dw]
            [mulify.core :as c]))


(defprocess config {:name                      "apikit-config"
                    :outbound-headers-map-name "outboundHeaders"
                    :disable-validations       false
                    :api                       "api.raml"
                    :http-status-var-name      "httpStatus"}
  :xml-real-ns "mulify.mule-apikit")

(defprocess router {:config-ref "apikit-router-config"} :xml-real-ns "mulify.mule-apikit")
(defprocess console {:config-ref "apikit-config"} :xml-real-ns "mulify.mule-apikit")
(defprocess flow-mappings {} :xml-real-ns "mulify.mule-apikit")
(defprocess flow-mapping {:resource-path "/" :action "get" :content-type "application/json"}
  :xml-real-ns "mulify.mule-apikit")


(s/fdef config
  :args (s/cat
          :attr (s/keys
                  :opt-un [::name ::outbound-headers-map-name ::disable-validations ::api ::http-status-var-name])))

(s/fdef router
  :args (s/cat :attr (s/keys :req-un [::config-ref])))


(def ^:dynamic *apikit-id*)

(defmacro on-apikit-listener [opts-map & body]
  (let [{:keys [id path port api-path output-mime-type]
         :or {path "/api/*"
              port "8081"
              api-path "api.raml"
              output-mime-type "application/json"}} opts-map]
    `(binding [*apikit-id* ~id]
       (let [id# (name ~id)
             apikit-flow-main-name# (str "apkit-flow-main-" id#)
             apikit-config-name# (str "apikit-config-" id#)
             http-listener-config-name# (str "http-listener-config-" id#)
             listen-host# "0.0.0.0"]
         (list
           (mulify.http/listener-config {:name http-listener-config-name#}
             (mulify.http/listener-connection {:host listen-host#
                                               :port ~port}))
           (mulify.apikit/config {:name apikit-config-name#
                                  :api ~api-path})
           (mulify.core/flow
             {:name apikit-flow-main-name#}
             (mulify.http/listener {:config-ref http-listener-config-name#
                                    :path ~path}
               (mulify.http/response {:status-code (mulify.dataweave/fx "vars.httpStatus")}
                 (mulify.http/headers (mulify.dataweave/fx "vars.outBoundHeaders"))))

             
             (mulify.apikit/router {:config-ref apikit-config-name#})
             (mulify.ee/transform
               (mulify.ee/message
                 (mulify.ee/set-payload
                   (str/join "\n" ["%dw 2.0" 
                                   (str "output " ~output-mime-type)
                                   "---"
                                   "payload"]))))

             (mulify.core/error-handler
               {:name "error-handler"}
               (mulify.core/on-error-propagate
                 {:type "APIKIT:BAD_REQUEST"}
                 (mulify.ee/transform
                   (mulify.ee/message
                     (mulify.ee/set-payload
                       "%dw 2.0\noutput application/json\n---\n{message: \"Bad request\"}"))
                   (mulify.ee/variables
                     (mulify.ee/set-variable {:variable-name "httpStatus"} "400"))))
               (mulify.core/on-error-propagate
                 {:type "APIKIT:NOT_FOUND"}
                 (mulify.ee/transform
                   (mulify.ee/message
                     (mulify.ee/set-payload
                       "%dw 2.0\noutput application/json\n---\n{message: \"Resource not found\"}"))
                   (mulify.ee/variables
                     (mulify.ee/set-variable {:variable-name "httpStatus"} "404"))))
               (mulify.core/on-error-propagate
                 {:type "APIKIT:METHOD_NOT_ALLOWED"}
                 (mulify.ee/transform
                   (mulify.ee/message
                     (mulify.ee/set-payload
                       "%dw 2.0\noutput application/json\n---\n{message: \"Method not allowed\"}"))
                   (mulify.ee/variables
                     (mulify.ee/set-variable {:variable-name "httpStatus"} "405"))))
               (mulify.core/on-error-propagate
                 {:type "APIKIT:NOT_ACCEPTABLE"}
                 (mulify.ee/transform
                   (mulify.ee/message
                     (mulify.ee/set-payload
                       "%dw 2.0\noutput application/json\n---\n{message: \"Not acceptable\"}"))
                   (mulify.ee/variables
                     (mulify.ee/set-variable {:variable-name "httpStatus"} "406"))))
               (mulify.core/on-error-propagate
                 {:type "APIKIT:UNSUPPORTED_MEDIA_TYPE"}
                 (mulify.ee/transform
                   (mulify.ee/message
                     (mulify.ee/set-payload
                       "%dw 2.0\noutput application/json\n---\n{message: \"Unsupported media type\"}"))
                   (mulify.ee/variables
                     (mulify.ee/set-variable {:variable-name "httpStatus"} "415"))))
               (mulify.core/on-error-propagate
                 {:type "APIKIT:NOT_IMPLEMENTED"}
                 (mulify.ee/transform
                   (mulify.ee/message
                     (mulify.ee/set-payload
                       "%dw 2.0\noutput application/json\n---\n{message: \"Not Implemented\"}"))
                   (mulify.ee/variables
                     (mulify.ee/set-variable
                       {:variable-name "httpStatus"}
                       "501"))))))
           ~@body)))))

(defn router-path [method path & [media-type apikit-config]]
  (-> (str/join ":" [(name method) path (get {:json "application/json"
                                              :xml "application/xml"
                                              :csv "application/csv"}
                                          media-type
                                          "")
                     (or apikit-config (str "apikit-config-" (name *apikit-id*)))])
    (str/replace "::" ":")
    (str/replace "/" "\\")))


(defmacro +get [path & body]
  `(c/flow {:name (router-path :get ~path)}
     ~@body))

(defmacro +post [path & body]
  `(c/flow {:name (router-path :post ~path)}
     ~@body))

(defmacro +put [path & body]
  `(c/flow {:name (router-path :put ~path)}
     ~@body))

(defmacro +patch [path & body]
  `(c/flow {:name (router-path :put ~path)}
     ~@body))

(defmacro +delete [path & body]
  `(c/flow {:name (router-path :delete ~path)}
     ~@body))
