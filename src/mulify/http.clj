(ns mulify.http
  (:require [clojure.data.xml :as dx]
            [clojure.string :as str]
            [mulify.dataweave :as dw]
            [mulify.tls :as tls]
            [silvur.util :refer [uuid edn->json]]
            [mulify.core :refer (defprocess >camel)]))



(defprocess listener-connection
  {:port "8081" :host "localhost" :protocol "HTTP"})

(defprocess listener-config {:name "listner_config"})

(defmulti listener-config* (fn [& {:keys [type key-path]}]
                             (if (or key-path (= type :https)) :https :http)))

(defmethod listener-config* :https [& {:keys [name host port key-type key-path key-password config-prefx]
                                       :or {name "https_listener_config"
                                            key-type "pkcs12"
                                            key-path "server.p12"
                                            host "0.0.0.0"
                                            port "8082"}}]
  (listener-config
   {:name name}
   (listener-connection
    (if config-prefx
      {:host "${https.host}" :port "${https.port}" :protocol "HTTPS"}
      {:host host :port port :protocol "HTTPS"})
    (tls/context
     (tls/trust-store {:type key-type})
     (tls/key-store
      {:type key-type
       :path key-path
       :key-password key-password
       :password key-password})))))


(defmethod listener-config* :http [& {:keys [name host port config-prefix]
                                      :or {name "http_listener_config" port "8081"}}]
  (->> (listener-connection (if config-prefix
                              {:host (str "${" config-prefix ".host}")
                               :port (str "${" config-prefix ".port}")}
                              {:host host
                               :port port}))
       (listener-config {:name name})))




(defprocess request-connection {:host "localhost" :port '_ :protocol "HTTP"})

(defprocess request-config {:name "request_config"})


(defmulti request-config* (fn [{:keys [type]}] type))

(defmethod request-config* :http [& {:keys [name port host config-prefix]
                                     :or {name "https_request_config" host "localhost" port "80"}}]
  (request-config {:name name}
                  (request-connection (if config-prefix
                                        {:protocol "HTTP"
                                         :host (str "${" config-prefix ".host}")
                                         :port (str "${" config-prefix ".port}")}
                                        {:protocol "HTTP" :host host :port port}))))

(defmethod request-config* :https [& {:keys [name port host config-prefix]
                                     :or {name "https_request_config" host "localhost" port "443"}}]
  (request-config {:name name}
                  (request-connection (if config-prefix
                                        {:protocol "HTTPS"
                                         :host (str "${" config-prefix ".host}")
                                         :port (str "${" config-prefix ".port}")}
                                        {:protocol "HTTPS" :host host :port port}))))

(defprocess response {:status-code "#[vars.httpStatus default 200]"})
(defprocess error-response {:status-code "#[vars.httpStatus default 500]"})
(defprocess body)
(defprocess listener {:path "/api/*" :config-ref "http_listner_config"})

(defprocess request {:method "GET" })

;:_
;; [:output-mime-type
;;  :output-encoding
;;  :streaming-strategy
;;  :path
;;  :url
;;  :follow-redirects
;;  :send-body-mode
;;  :request-streaming-mode
;;  :response-timeout
;;  :body
;;  :headers
;;  :uri-params
;;  :query-params
;;  :send-correlation-id
;;  :correlation-id
;;  :response-validator
;;  :target
;;  :target-value
;;  :reconnection-strategy]
(defprocess query-params :update-fn dx/cdata)
(defprocess uri-params :update-fn dx/cdata)

(defprocess headers :update-fn dx/cdata)

(defprocess load-static-resource {:resource-base-path "${app.home}/public"})

(defn listener* [{:keys [config-ref path headers-content-success cors?]
                  :or {config-ref "listener_config" path "/api/*"
                       cors? true
                       headers-content-success (dw/fx "attributes.headers default {}")}}]
  ;; NOTES: header name must be small case since MuleSoft uses small case.
  (listener
   {:config-ref config-ref, :path path}
   (response
    {:status-code "#[attributes.httpStatus default 200]"}
    (headers (if cors?
               headers-content-success
               (dw/fx "%dw 2.0
                       import mergeWith from dw::core::Objects
                       output java
                       ---
                      ((attributes.headers default {}) mergeWith {
                        'access-control-allow-origin': '*',
                        'access-control-allow-methods': '*', 
                        'access-control-allow-headers': '*'
                      }) "))))

    
   (error-response
    {:status-code "#[vars.httpStatus default 500]"}
    (body "#[payload]")
    (headers "#[vars.headers default {}]"))))
