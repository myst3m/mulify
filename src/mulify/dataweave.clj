(ns mulify.dataweave
  (:require [clojure.data.xml :as dx]
            [jsonista.core :as j]
            [jsonista.tagged :as jt]
            [clojure.string :as str]
            [mulify.dataweave :as dw]))

(def mapper
  (j/object-mapper {:encode-key-fn true
                    :decode-key-fn true
                    :modules [(jt/module {:handlers {clojure.lang.Symbol {:tag "!sym"
                                                                          :encode jt/encode-str
                                                                          :decode symbol}}})]}))

(defn fx
  ([body]
   (str "#[" (if (map? body)
               (str/replace (j/write-value-as-string body mapper) #"\[\"!sym\",\"(.*?)\"\]" "$1")
               body) "]"))
  ([output body]
   (str "#[" "output " (name output) " --- " (if (map? body)
                                               (str/replace (j/write-value-as-string body mapper) #"\[\"!sym\",\"(.*?)\"\]" "$1")
                                               (symbol body)) "]")))

(defn wrap [^String body & [output]]
  (if (.matches body "^#.*")
    body
    (str "%dw 2.0\n"
         "output " (or output "application/json\n")
         "---\n"
         body)))



(defn csv->csv
  ([]
   (csv->csv {} "payload"))
  ([opts]
   (csv->csv opts "payload"))
  ([{:keys [header streaming deferred] :or {header true}} body]
   (cond-> "%dw 2.0"
     true      (str " input payload csv")
     true      (str " header=" (true? header))
     streaming (str ",streaming=" (true? streaming))
     true      (str " output csv")
     true      (str " header=" (true? header))
     deferred  (str ",deferred=" (true? deferred))
     true      (str " --- " (or body "payload")))))

(defn csv->json
  ([]
   (csv->json {} "payload"))
  ([body]
   (csv->json {} body))
  ([{:keys [header streaming deferred] :or {header true}} body]
   (cond-> "%dw 2.0"
     true      (str " input payload csv")
     true      (str " header=" (true? header))
     streaming (str ",streaming=" (true? streaming))
     true      (str " output json")
     deferred  (str ",deferred=" (true? deferred))
     true      (str " --- " body))))
