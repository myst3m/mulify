(ns mulify.admin.openssl
  (:require [clojure.java.shell :refer [sh with-sh-dir with-sh-env]]
            [clojure.string :as str]
            [clojure.java.io :as io]
            [babashka.fs :as fs]))

(def ^:dynamic *work-dir* (str "tls-keys" (str/replace (str (gensym)) #"G__" "-")))
(def ^:dynamic *ca-root* "ca")

(defn work-dir [& paths]
  (str/join "/" (cons *work-dir* paths)))

(defn ca-dir [& paths]
  (str/join "/" (cons *ca-root* paths)))

(def ^:dynamic *debug* false)
(def ^:dynamic *dry* false)

(defn debug! [& [on-or-off]]
  (alter-var-root #'*debug* (constantly (or (nil? on-or-off) (#{true :on} on-or-off)))))

(defn set-ca-root! [path]
  (alter-var-root #'*ca-root* (constantly path)))


(defn gen-file [type cmd-array file-name]
  (let [target-path (io/file *work-dir* file-name)]
    (io/make-parents target-path)
    (when *debug*
      (println (str/join " " cmd-array)))
    
    (if *dry*
      (str/join " " cmd-array)
      (let [
            {:keys [exit out err]} (apply sh cmd-array)]
        (if (= 0 exit)
          (do (println "created" (name type) (str target-path))
              (spit target-path out))
          (println err))))))


(defn gen-private-key [& {:keys [length passout name]
                          :or {length 2048
                               name "private-key.pem"}}]
  
  (let [cmd-array (cond-> ["openssl" "genrsa" ]
                    passout (concat ["-aes256" "-passout" (str "pass:" passout)])
                    length (concat [(str length)]))]
    (gen-file :private-key cmd-array name)))





;;;; General

(defn gen-certificate-signing-request [& {:keys [key-file passin config subject cn name ]
                                          :or {key-file "private-key.pem"
                                               cn "localhost"
                                               name "cert.csr"}}]
  (let [cmd-array (cond-> ["openssl" "req" "-new"]
                    config (concat ["-config" config])
                    key-file (concat ["-key" (work-dir key-file)])
                    passin (concat ["-passin" (str "pass:" passin)])
                    :exec (concat ["-subj" (or subject (str "/C=JP/ST=Tokyo/L=Chiyoda/O=OrgX/OU=IT/CN=" cn))]))]
    (gen-file :certificate-request cmd-array name)))



(defn self-certify [& {:keys [key-file csr-file passin days name]
                       :or {key-file "private-key.pem"
                            days 365
                            name "x509.crt"
                            csr-file "cert.csr"}}]
  
  (let [cmd-array (cond-> ["openssl" "x509" "-req"]
                                 key-file (concat ["-signkey" (work-dir key-file)])
                                 days (concat ["-days" (str days)])
                                 csr-file (concat ["-in" (work-dir csr-file)])
                                 :always (concat ["-passin" (str "pass:" passin)]))]
    (gen-file :self-certificate cmd-array name)))




;;;; CA

(defn gen-ca-cert [& {:keys [days ca-root ca-config-path key-file subject cn name]
                      :or {cn "localhost"
                           name "ca-cert.pem"
                           key-file "private-key.pem"}}]
  (let [cmd-array (cond-> ["openssl" "req" "-new" "-x509"]
                    days (concat ["-days" days])
                    ca-config-path (concat ["-config" ca-config-path])
                    key-file (concat ["-key" (work-dir key-file)])
                    :always (concat ["-subj" (or subject (str "/C=JP/ST=Tokyo/L=Chiyoda/O=OrgX/OU=CA/CN=" cn))]))]
    (gen-file :ca-cert cmd-array name)))

(defn ca-certify [& {:keys [ca-config-path csr-file days ca-key-file ca-cert-file name san]
                     :or {name "x509.crt"
                          csr-file (str "cert.csr")}}]
  (let [san-file (str "san-" (gensym))
        cmd-array (cond-> ["openssl" "ca"]
                    ca-key-file (concat ["-keyfile" ca-key-file])
                    ca-cert-file (concat ["-cert" ca-cert-file])
                    csr-file (concat ["-in" (work-dir csr-file)])
                    days (concat ["-days" (str days)])
                    san (concat ["-extfile" san-file])
                    :always (concat ["-config" (or ca-config-path (ca-dir "ca.cnf"))])
                    :always (concat ["-notext"])
                    :always (concat ["-batch"]))]
    (with-sh-env {:CA_DIR (or *ca-root* (fs/parent ca-config-path))}
      (spit san-file (str "subjectAltName = " san))
      (gen-file :certificate cmd-array name))
    ;;(when san (fs/delete san-file))
    ))

;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Sequence
;;; Use this function to create self cert with one stop
(defn gen-self-certification [& {:keys [key-pass ca-config-path]}]
  (gen-private-key :passout key-pass)
  (gen-certificate-signing-request :passin key-pass :ca-config-path ca-config-path)
  (self-certify :passin key-pass))



;; X509
(defn x509->pkcs12 [& {:keys [in-file key-file passin passout p12-out]
                       :or {in-file "x509.crt"
                            key-file "private-key.pem"}}]
  (let [p12-out (or p12-out (->> (-> (str/split in-file #"\.")
                                     (butlast )
                                     (str/join)
                                     (str ".p12"))))
        cmd-array (cond-> ["openssl" "pkcs12" "-export"]
                    in-file (concat ["-in" (work-dir in-file)])
                    key-file (concat ["-inkey" (work-dir key-file)])
                    :exec (concat ["-out" (work-dir p12-out)])
                    :exec  (concat ["-passin" (str "pass:" passin)])
                    :exec  (concat ["-passout" (str "pass:" passout)]))]
    (if *dry*
      (println cmd-array)
      (let [{:keys [exit out err]} (apply sh cmd-array)]
        (if (= 0 exit)
          (println "created pkcs12:" p12-out)
          (println err))))))


(defn gen-self-certification-pkcs12 [& {:keys [key-pass store-pass] :as args}]
  ;; Java does not support the difference of keypass and storepass.
  (apply gen-self-certification args)
  (x509->pkcs12 :passin key-pass :passout (or store-pass key-pass)))

;; PKCS12

(defn pkcs12->x509 [& {:keys [passin passout p12-file keys? certs? cacerts?]}]
  (let [cmd-array (cond-> ["openssl" "pkcs12" "-nodes"]
                    p12-file (concat ["-in" (work-dir p12-file)])
                    (some? keys?) (concat ["-nocerts"])
                    (some? certs?) (concat ["-nokeys"])
                    (some? cacerts?) (concat ["-cacerts" "-nokeys" "-nocerts"])
                    :exec  (concat ["-passin" (str "pass:" passin)])
                    :exec  (concat ["-passout" (str "pass:" passout)])
                    :exec (concat ["-info"]))]
    
    (if *dry*
      (println (str/join " " cmd-array))
      (let [{:keys [exit out err]} (apply sh cmd-array)]
        (if (= 0 exit)
          out
          (println err))))))

(defn cert-info [pem-path & [type]]
  (let [cmd-array ["openssl" (if (or (= type :csr) (re-find #".*csr" pem-path))
                               "req"
                               "x509")
                   "-in" pem-path "-text"]
        {:keys [out err exit]} (apply sh cmd-array)]
    (cond
      (= 0 exit) out
      (and (not= 0 exit) (not= type :csr)) (cert-info pem-path :csr))))

(defn ez-cert [& {:keys [cn ca-config-path]}]
  (let [cn (or cn "*.mule-dev.com")]
    (binding [*debug* true]
           (gen-private-key)
           (gen-certificate-signing-request :cn cn)
           (ca-certify :san (str "DNS:" cn) :ca-config-path ca-config-path))))


;; keytool -genkey -keyalg RSA -alias <key-alias> -keystore <keystore-name>.jks
;; keytool -export -alias <key-alias> -keystore <keystore-name>.jks -file <certificate-name>.cer

(defn build-ca [& [root-dir]]
  (binding [*debug* true
            *work-dir* (or root-dir "ca")]
    (io/make-parents (work-dir "."))
    (let [index-file (work-dir "index.txt")
          serial-file (work-dir "serial")
          certs-dir (work-dir "certs" ".keep")
          ca-conf-path (io/file (work-dir  "ca.cnf"))]
      (io/make-parents certs-dir)    
      (spit serial-file "01")
      (spit index-file "")
      (io/copy (slurp (io/resource "ca.cnf")) ca-conf-path)
      (with-sh-env {:CA_DIR *work-dir*}
        (gen-private-key :name "ca-key.pem")
        (gen-ca-cert :name "certs/ca-cert.pem"
                     :ca-config-path (str ca-conf-path)
                     :key-file "ca-key.pem"
                     :subject "/C=JP/ST=Tokyo/L=Chiyoda/O=CA/OU=CA/CN=CA")))))


