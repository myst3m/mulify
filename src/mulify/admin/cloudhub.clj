(ns mulify.admin.cloudhub
  (:gen-class)
  (:refer-clojure :exclude [error-handler])
  (:require [silvur.http :as http]
            [silvur.util :refer [edn->json json->edn]]
            [clojure.java.io :as io]
            [clojure.string :as str]))

(defonce default-context (atom {}))

(defn login [user password]
  (-> @(http/post "https://anypoint.mulesoft.com/accounts/login"
                  {:headers {"Content-Type" "application/json"}
                   :body (edn->json {:username user :password password})})
      :body
      json->edn))

(defn login! [user password]
  (reset! default-context (login user password)))

(defn me
  ([]
   (when-let [{:keys [access_token]} @default-context]
     (me access_token)))
  ([token & paths]
   (-> (http/get "https://anypoint.mulesoft.com/accounts/api/me"
                 {:headers {"Authorization" (str "Bearer " token)}})
       deref
       :body
       json->edn)))

(defn environment
  ([]
   (->> (me)
        :user
        :memberOfOrganizations
        (mapcat environment)))
  ([{:keys [id name] :as me}]
   (map #(assoc % :organizationName name) (environment (:access_token @default-context) id)))
  ([token org-id]
   (-> (format "https://anypoint.mulesoft.com/accounts/api/organizations/%s/environments" org-id)
       (http/get  {:headers {"Authorization" (str "Bearer " token)}})
       deref
       :body
       json->edn
       :data)))

(defn set-default-environment! [org-name env-name]
  (swap! default-context conj (->> (environment)
                                   (filter #(and (re-find (re-pattern org-name) (:organizationName %))
                                                 (re-find (re-pattern env-name) (:name %))))
                                   (first)))
  )
(defn applications
  ([]
   (->> (environment)
        (map :id)
        (map applications)))
  ([env-id]
   (applications (:access_token @default-context) env-id))
  ([token env-id]
   (-> (format "https://anypoint.mulesoft.com/cloudhub/api/v2/applications")
       (http/get  {:headers {"Authorization" (str "Bearer " token)
                             "x-anypnt-env-id" env-id}})
       deref
       :body
       json->edn)))

(defn deploy
  ([jar-path]
   (deploy (last (re-find #"(.*).jar$" (.getName (io/file jar-path)))) jar-path))
  ([app-name jar-path]
   (let [{:keys [access_token organizationName name ]} @default-context]
     (deploy app-name
             access_token
             organizationName
             name
             jar-path)))
  ([app-name token org-name env-name jar-path]
   (let [{:keys [name clientId organizationId id] :as e} (first (filter #(and (= org-name (:organizationName %))
                                                                              (= env-name (:name %))) (environment)))]
     (-> (format "https://anypoint.mulesoft.com/cloudhub/api/v2/applications")
         (http/post {:headers {"Authorization" (str "Bearer " token)
                               "Content-Type" "multipart/form-data"
                               "x-anypnt-env-id" id}
                     :multipart [{:name "file"
                                  :content (io/file jar-path)
                                  :filename (.getName (io/file jar-path))}
                                 {:name "autoStart" :content "true"}
                                 {:name "appInfoJson"
                                  :content
                                  (slurp (edn->json {:persistentQueues false,
                                                     :monitoringAutoRestart true,
                                                     :region "us-east-2",
                                                     :muleVersion {:version "4.4.0"},
                                                     :objectStoreV1 false,
                                                     :monitoringEnabled true,
                                                     :loggingNgEnabled true,
                                                     :domain app-name
                                                     :workers {:amount 1,
                                                               :type
                                                               {:name "Micro",
                                                                :memory "500 MB memory",
                                                                :cpu "0.1 vCores"}}
                                                     
                                                     }))}]})
         (deref)))))

(defn delete-application
  ([app-name]
   (let [{:keys [access_token organizationName name]} @default-context]
     (delete-application app-name access_token organizationName name)))
  ([app-name token org-name env-name]
   (let [{:keys [id] :as e} (or @default-context
                                (->> (environment)
                                     (filter #(and (= org-name (:organizationName %))
                                                   (= env-name (:name %))))
                                     (first)))]
     (if id
       (-> (format "https://anypoint.mulesoft.com/cloudhub/api/v2/applications")
           (http/put {:headers {"Authorization" (str "Bearer " token)
                                 "Content-Type" "application/json"
                                 "x-anypnt-env-id" id}
                       :body (edn->json {:action "DELETE"
                                         :domains [app-name]})})
           (deref))
       (println "No env id named" env-name)))))
