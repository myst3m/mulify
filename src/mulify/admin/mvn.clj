(ns mulify.admin.mvn
  (:require [clojure.java.shell :refer [sh]]
            [clojure.string :as str]
            [mulify.utils :as mu]
            [silvur.util :refer [json->edn edn->json edn->xml]]
            [clojure.data.xml :as xml]
            [babashka.fs :as fs]
            [clojure.java.io :as io]
            [mulify.ee :as ee]
            [mulify.utils :as utils]))

(defn package []
  (println (:out (apply sh (str/split "mvn package" #" ")))))




