(ns mulify.admin.jdbc
  (:require [next.jdbc :as jdbc]
            [next.jdbc.result-set :as rs]
            [clojure.data.xml :as xml]
            [reitit.core :as r]
            [reitit.ring :as ring]
            [reitit.openapi :as openapi]
            [clojure.string :as str]
            [camel-snake-kebab.core :as csk]
            [camel-snake-kebab.extras :as cske]))

(def db {:dbtype "mysql"
         :dbname "db"
         :user "user"
         :host "localhost"
         :password "password"
         :port 3306})

(def db {:dbtype "mysql"
         :dbname "k1"
         :user "mule"
         :host "localhost"
         :password "mule123"
         :port 3306})

(defonce ^:dynamic *ds* (atom nil))
(defonce ^:dynamic *db-spec* (atom nil))

(defn set-default-datasource! [& {:keys [dbtype dbname user host password port] :as db-spec
                                  :or {dbtype "mysql" port 3306}}]
  (reset! *db-spec* (dissoc db-spec :password))
  (reset! *ds* (jdbc/get-datasource db-spec)))

(defn get-spec []
  @*db-spec*)

(defn get-database-meta-data []
  (with-open [con (jdbc/get-connection @*ds*)]
    (-> (.getMetaData con)
        (.getTables nil nil nil (into-array ["TABLE"]))
        (rs/datafiable-result-set )
        (->> (map #(select-keys % [:TABLE_NAME]))))))

(defn get-table-meta-data
  ([table]
   (get-table-meta-data (:user (get-spec)) table))
  ([schema table]
   (with-open [con (jdbc/get-connection @*ds*)]
     (-> (.getMetaData con)
         (.getColumns nil (name schema) (name table) nil)
         (rs/datafiable-result-set)
         (->> (map #(select-keys % [:COLUMN_NAME :TYPE_NAME :COLUMN_SIZE :DATA_TYPE :IS_NULLABLE])))))))

(defn get-keys-meta-data [schema table]
  (with-open [con (jdbc/get-connection @*ds*)]
    (prn con)
    (-> (.getMetaData con)
        ;;(.getPrimaryKeys nil schema table)
        ;;(rs/datafiable-result-set)
        ;;(->> (map #(select-keys % [:COLUMN_NAME :TYPE_NAME :COLUMN_SIZE])))
        )))



(defn gen-csdl [schema table]
  (let [properties (->> (get-table-meta-data schema table)
                        (map #(xml/element :Property (conj {:Name (:COLUMN_NAME %)}
                                                           (condp = (:TYPE_NAME %)
                                                             "INT" {:Type (str "Edm.Int" (* (:DATA_TYPE %) 8))}
                                                             "DATE" {:Type (str "Edm.DateTime")} 
                                                             "VARCHAR" {:Type "Edm.String"
                                                                        :MaxLength (:COLUMN_SIZE %)
                                                                        :Unicode "true"})
                                                           (when (= (:IS_NULLABLE %) "YES")
                                                             {:Nullable "true"})))))]
    (xml/emit-str 
     (apply xml/element :EntityType {:Name table}
            properties))
    ))


;;; Routing
(def type-conversion-rules {:VARCHAR string?
                            :INT int?
                            :SMALLINT int?})
(defn gen-routes [& tables]
  (->> tables
       (reduce (fn [r t]
                 (conj r
                       [(str "/api/" (str/lower-case t))
                        {:get
                         {:responses
                          {200
                           {:body (->> (get-table-meta-data t)
                                       (map (fn [{:keys [COLUMN_SIZE TYPE_NAME COLUMN_NAME]}]
                                              [(str/lower-case COLUMN_NAME) (type-conversion-rules (keyword TYPE_NAME))]))
                                       (into {})
                                       (cske/transform-keys csk/->kebab-case-keyword))}}}}]))
               [])))


(def app (ring/ring-handler
          (ring/router
           [["/openapi.json"
             {:get {:handler (openapi/create-openapi-handler)
                    :openapi {:info {:title "my nice api" :version "0.0.1"}}
                    :no-doc true}}]
            ["/api/uc10"
             {:post
              {:parameters {:body {:name string?}}
               :handler (fn [_] {:status 200})
               :responses
               {200
                {:body
                 {:moshikomi-bango string?,
                  :daihyo-moshikomi-bango string?,
                  :moshikomi-teisei-kubun int?,
                  :koji-gyosha-id string?,
                  :api-renkei-kubun int?}}}}}]])))


