(ns mulify.admin.project
  (:require [babashka.fs :as fs]
            [silvur.util :refer [json->edn edn->json edn->xml]]
            [clojure.java.io :as io]))

(defn gen-dot-project [root-path & {:keys [name] :or {name "hello-world"}}]
  (->> {:projectDescription
        {:projects {},
         :buildSpec
         {:buildCommand
          [{:arguments {}, :name "org.eclipse.jdt.core.javabuilder"}
           {:arguments {},
            :name "org.mule.tooling.core.muleStudioBuilder"}]},
         :name name,
         :comment {},
         :natures
         {:nature
          ["org.mule.tooling.core.muleStudioNature"
           "org.eclipse.jdt.core.javanature"]}}}
       (edn->xml)
       (spit (io/file root-path ".project"))))

(defn gen-pom-xml [root-path & {:keys [name group-id]
                                :or {name "hello-world" gorup-id "my-group"}}]
  (->> {:project
        {:properties
         {:project.build.sourceEncoding "UTF-8",
          :mule.maven.plugin.version "3.6.3",
          :app.runtime "4.4.0-20220622",
          :project.reporting.outputEncoding "UTF-8"},
         :groupId group-id,
         :packaging "mule-application",
         :name name,
         :-xsi:schemaLocation
         "http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd",
         :-xmlns "http://maven.apache.org/POM/4.0.0",
         :repositories
         {:repository
          [{:layout "default",
            :name "Anypoint Exchange",
            :id "anypoint-exchange-v3",
            :url "https://maven.anypoint.mulesoft.com/api/v3/maven"}
           {:layout "default",
            :name "MuleSoft Releases Repository",
            :id "mulesoft-releases",
            :url "https://repository.mulesoft.org/releases/"}]},
         :-xmlns:xsi "http://www.w3.org/2001/XMLSchema-instance",
         :build
         {:plugins
          {:plugin
           [{:groupId "org.apache.maven.plugins",
             :artifactId "maven-clean-plugin",
             :version "3.0.0"}
            {:groupId "org.mule.tools.maven",
             :artifactId "mule-maven-plugin",
             :extensions "true",
             :version "${mule.maven.plugin.version}"}]}},
         :artifactId name,
         :pluginRepositories
         {:pluginRepository
          {:layout "default",
           :name "MuleSoft Releases Repository",
           :id "mulesoft-releases",
           :snapshots {:enabled "false"},
           :url "https://repository.mulesoft.org/releases/"}},
         :version "1.0.0-SNAPSHOT",
         :dependencies
         {:dependency
          [{:groupId "org.mule.connectors",
            :classifier "mule-plugin",
            :artifactId "mule-http-connector",
            :version "1.7.1"}
           {:groupId "org.mule.connectors",
            :classifier "mule-plugin",
            :artifactId "mule-sockets-connector",
            :version "1.2.2"}]},
         :modelVersion "4.0.0"}}
       (edn->xml)
       (spit (io/file root-path "pom.xml"))))

(defn gen-dot-classpath [root-path]
  (->> {:classpath
        {:classpathentry
         [{:-path
           "MULE_LIB/org.mule.connectors/mule-sockets-connector/1.2.2",
           :-kind "con"}
          {:-path "MULE_LIB/org.mule.connectors/mule-http-connector/1.7.1",
           :-kind "con"}
          {:-path "org.eclipse.jdt.launching.JRE_CONTAINER", :-kind "con"}
          {:-exported "true",
           :-path "MULE_RUNTIME/org.mule.tooling.server.4.4.0.ee",
           :-kind "con"}
          {:-path "src/main/mule", :-kind "src"}
          {:-path "src/main/java", :-kind "src"}
          {:-path "src/main/resources", :-kind "src"}
          {:-output "target/test-classes",
           :-path "src/test/java",
           :-kind "src"}
          {:-output "target/test-classes",
           :-path "src/test/resources",
           :-kind "src"}
          {:-path "src/test/munit", :-kind "src"}
          {:-path "target/classes", :-kind "output"}]}}
       (edn->xml)
       (spit (io/file root-path ".classpath"))))

(defn gen-mule-artifact [root-path]
  (->> {:minMuleVersion "4.4.0"}
       edn->json
       slurp
       (spit (io/file root-path "mule-artifact.json"))))

(defn gen-clj-api [root-path & {:keys [name] :or {name "hellow-world"}}]
  (let [header (list (symbol (str 'ns " " name ".core"))
                     '(:gen-class)
                     '(:refer-clojure :exclude [error-handler])
                     '(:require [clojure.string :as str]
                                [clojure.java.io :as io]
                                [clojure.tools.cli :refer (parse-opts)]
                                [silvur.datetime :refer (datetime datetime*)]
                                [silvur.util :refer (json->edn edn->json)]
                                [mulify.utils :as utils]
                                [mulify
                                 [core :as c]
                                 [http :as http]
                                 [os :as os]
                                 [vm :as vm]
                                 [ee :as ee]
                                 [db :as db]
                                 [tls :as tls]
                                 [dataweave :as dw]
                                 [batch :as batch]
                                 [apikit :as ak]
                                 [apikit :as ak-odata]
                                 [jms :as jms]
                                 [wsc :as wsc]
                                 [anypoint-mq :as amq]
                                 [api-gateway :as gw]
                                 [generic]
                                 [file :as f]]))
        body '(def mule-app (mulify.core/mule
                             (mulify.http/listener-config
                              {:name "listener-config"}
                              (mulify.http/listener-connection {:host "0.0.0.0", :port "8081"}))
                             (mulify.core/flow
                              {:name "main"}
                              (mulify.http/listener* {:config-ref "listener-config" :path "*" } )
                              (c/logger* :info (dw/fx "payload"))
                              (ee/transform* {:dw "payload"}))))
        footer (list 'utils/transpile 'mule-app ':out (str "src/main/mule/" name ".xml"))
        path (io/file root-path "src" name "core.clj")]
    (io/make-parents path)
    (->> (with-out-str
           (clojure.pprint/pprint header)
           (println )
           (clojure.pprint/pprint (symbol ";; Main Flow"))
           (println )
           (clojure.pprint/pprint body)
           (println )
           (clojure.pprint/pprint (symbol ";; Transpile"))
           (println )
           (clojure.pprint/pprint footer))
         (spit path))))

(defn gen-deps-edn [root-path & {:keys [name group-id]}]
  (->> (with-out-str
         (-> {:paths ["src" "resources"],
              :deps {'org.clojure/clojure {:mvn/version "1.11.1"},
                     'org.clojure/tools.cli {:mvn/version "1.0.206"},
                     'io.gitlab.myst3m/silvur {:git/tag "2.2.8" ':git/sha "328aea2"},
                     'io.gitlab.myst3m/mulify {:mvn/version "0.3.0"},
                     'org.clojure/spec.alpha {:mvn/version "0.3.214"}},
              :aliases {:test {:extra-paths ["test"],
                               :extra-deps {'org.clojure/test.check {:mvn/version "0.10.0"}}},
                        :runner {:extra-deps {'com.cognitect/test-runner {:git/url "https://github.com/cognitect-labs/test-runner",
                                                                          :sha "f7ef16dc3b8332b0d77bc0274578ad5270fbfedd"}},
                                 :main-opts ["-m"
                                             "cognitect.test-runner"
                                             "-d"
                                             "test"]},
                        :build {:main-opts ["-m" "silvur.build"]},
                        :deploy {:main-opts ["-m" "silvur.build" "deploy"]},
                        :javac {:main-opts ["-m" "silvur.build" "javac"]}
                        :uberjar {:main-opts ["-m"
                                              "silvur.build"
                                              "uberjar"
                                              "-a"
                                              (str group-id "/" name)
                                              "-m"
                                              (str name ".core")
                                              "--aot"
                                              "-v"
                                              "0.0.1-SNAPSHOT"]}},
              :mvn/repos {"mule-release" {:url "http://repository.mulesoft.org/releases/"},
                          "clojars" {:url "https://clojars.org/repo"},
                          "jitpack" {:url "https://jitpack.io"}}}
             (clojure.pprint/pprint)))
       (spit (io/file root-path "deps.edn"))))

(defn init-project [root-path group-id project-name]
  (doseq [d [".mule" ".settings" "src/main/java" "src/main/mule" "src/main/resources"
             "src/test/munit" "src/test/java"  "src/test/resources" ]]
    (fs/create-dirs (io/file root-path d))
    (gen-dot-project root-path :name project-name)
    (gen-mule-artifact root-path)
    (gen-dot-classpath root-path)
    (gen-pom-xml root-path :name project-name :group-id group-id)
    (gen-dot-project root-path :name project-name)
    (gen-clj-api root-path :name project-name)
    (gen-deps-edn root-path :name project-name :group-id group-id)))

