(ns mulify.jms
  (:refer-clojure :exclude [read])
  (:require [clojure.data.xml :as dx]
            [clojure.string :as str]
            [mulify.core :refer [defprocess]]
            [mulify.dataweave :as dw]
            [mulify.core :refer (defprocess >camel)]))


(defprocess config {:name "JMS_Config"})

(defprocess active-mq-connection)
(defprocess factory-configuration)

(comment
  (mulify.jms/config
  {:name "JMS_Config"}
  (mulify.jms/active-mq-connection
   (mulify.jms/factory-configuration))))
