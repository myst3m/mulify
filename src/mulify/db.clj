(ns mulify.db
  (:refer-clojure :exclude [update])
  (:require [clojure.data.xml :as dx]
            [silvur.util :refer [uuid]]
            [silvur.log :as log]
            [clojure.string :as str]
            [mulify.core :refer (defprocess >camel)]
            [clojure.spec.alpha :as s]
            [mulify.dataweave :as dw]
            [clojure.spec.test.alpha :as t]
            [mulify.core :as c]))

(defprocess config)
(defprocess sql :update-fn dx/cdata)

(defprocess input-parameters :update-fn dx/cdata)
(defprocess output-parameters)
(defprocess in-out-parameters)
(defprocess in-out-parameter)
(defprocess output-parameter)

(defprocess stored-procedure {:config-ref "Database_Config"})

(defprocess my-sql-connection {:host "localhost" :port 3306})
(defprocess oracle-connection {:host "localhost" :port 1521 :service-name "XEPDB1"})

(defprocess pooling-profile {:max-pool-size "10" :min-pool-size "1"})

(defprocess select {:config-ref "Database_Config"})
(defprocess update {:config-ref "Database_Config"})
(defprocess insert {:auto-generate-keys true :config-ref "Database_Config"})
(defprocess delete {:config-ref "Database_Config"})
(defprocess query-single {:config-ref "Database_Config"})
(defprocess bulk-insert {:config-ref "Database_Config"})

(defprocess listener {:config-ref "Database_Config"})
(defprocess execute-script {:file "ddl.sql" :config-ref "Database_Config"})


;; -------------------- Spec ----------------------

;;; Config
(s/fdef config
  :args (s/cat
          :attr (s/? (s/keys :req-un [::name]))
          :conn-config #(-> % :tag #{::my-sql-connection ::oracle-connection})))

(s/def ::dbms-config (s/keys :req-un [::host ::user ::password ::database] :opt-un [::port]))

(s/fdef my-sql-connection
  :args (s/cat :impl (s/keys :req-un [::host ::user ::password ::database] :opt-un [::port])))

(s/fdef oracle-connection
  :args (s/cat :impl (s/keys :req-un [::host ::user] :opt-un [::port ::instance ::service-name])))

;; SQL

(s/def ::cdata (partial instance? clojure.data.xml.node.CData))

;; (s/fdef sql
;;   :args (s/cat :body ::cdata))

(s/fdef input-parameters
  :args (s/alt
          :string string?
          :body ::cdata))

;;; Select
(s/fdef select
  :args (s/cat
          :attr (s/? (s/keys :req-un [::config-ref]))
          :sql #(-> % :tag #{::sql})
          :params (s/? #(-> % :tag #{::input-parameters}))
          ))

(s/fdef bulk-insert
  :args (s/cat
         :attr (s/? (s/keys :req-un [::config-ref]))
         :sql #(-> % :tag #{::sql})))

(defn- get-config-name [n]
  (-> (filter #(and (#{'mulify.db :db 'db} (:class %)) (= (keyword (:id %)) (keyword n))) c/global-configs)
    first 
    :id
    (as-> id
        (or id (throw (ex-info "Not found config for database in global-configuration"
                        {:given-id n}))))
    (->> (str "database-config-"))))

;; -------------------- Helper ----------------------
(defn select* [config-id query-body & params]
  ;; Assume sf-config is XML tree
  (select {:config-ref (get-config-name config-id)}
    (sql (dx/cdata query-body))
    (input-parameters (dw/fx (into {} (map vec (partition-all 2 params)))))))

(defn insert* [config-id query-body & params]
  ;; Assume sf-config is XML tree
  (insert {:config-ref (get-config-name config-id)}
    (sql (dx/cdata query-body))
    (input-parameters (dw/fx (into {} (map vec (partition-all 2 params)))))))

(defn delete* [config-id query-body & params]
  ;; Assume sf-config is XML tree
  (delete {:config-ref (get-config-name config-id)}
    (sql (dx/cdata query-body))
    (input-parameters (dw/fx (into {} (map vec (partition-all 2 params)))))))

(defn update* [config-id query-body & params]
  ;; Assume sf-config is XML tree
  (mulify.db/update {:config-ref (get-config-name config-id)}
    (sql (dx/cdata query-body))
    (input-parameters (dw/fx (into {} (map vec (partition-all 2 params)))))))


;; Helper

;; (defn insert* [{:keys [config-ref sql-content input-parameters-content]
;;                 :or {sql-content "#[payload]"
;;                      input-parameters-content "#[attributes.queryParams"}}]
;;   (insert
;;    {:config-ref config-ref}
;;    (sql sql-content)
;;    (input-parameters input-parameters-content)))


(defmulti config* (fn [{:keys [type id config-prefix host port user password database]}]
                    type ))
(defmethod config* :mysql [{:keys [type id config-prefix host port user password database]
                            :or {host "localhost"
                                 port "3306"}}]
  (config
   {:name (get-config-name id)}
   (my-sql-connection
    (if config-prefix
      {:host (str "${" config-prefix ".host}"),
       :port (str "${" config-prefix ".port}")
       :user (str "${" config-prefix ".user}")
       :password (str "${" config-prefix ".password}")
       :database (str "${" config-prefix ".database}")}
      {:host host
       :port port
       :user user
       :password password
       :database database}))))

(defmethod config* :oracle [{:keys [type name config-prefix host port user password database]
                            :or {name "Database_Config"
                                 host "localhost"
                                 port "1521"}}]
  (config {:name name}
    (oracle-connection
      (if config-prefix
        {:host (str "${" config-prefix ".host}"),
         :port (str "${" config-prefix ".port}")
         :user (str "${" config-prefix ".user}")
         :password (str "${" config-prefix ".password}")
         :database (str "${" config-prefix ".database}")}
        {:host host
         :port port
         :user user
         :password password
         :database database}))))


(defmethod config* :default [_]
  (throw (Exception. "No database definition")))

(t/instrument)
