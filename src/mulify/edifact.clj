(ns mulify.edifact
  (:refer-clojure :exclude [write read])
  (:require [clojure.string :as str]
            [clojure.java.io :as io]
            [clojure.xml :as xml]
            [clojure.zip :as zip]
            [clojure.data.xml :as dx]
            [silvur.datetime]
            [silvur.util :refer [uuid]]
            [taoensso.timbre :as log]
            [mulify.core :refer (defprocess >camel)]
            [clojure.spec.alpha :as s]
            [clojure.spec.test.alpha :as t]
            [clojure.spec.gen.alpha :as gen]))

(defprocess config {:name "a":line-ending "LF"}
  :key-fn (fn [m]
            (update-keys (>camel m) #(keyword (str/replace (clojure.core/name %) #"[uU]na" "UNA")))))
(defprocess write)
(defprocess read)



