(ns mulify.wsc
  (:require [clojure.data.xml :as dx]
            [clojure.string :as str]
            [mulify.dataweave :as dw]
            [silvur.util :refer [uuid edn->json]]
            [mulify.core :refer (defprocess >camel)]))

(defprocess config {:name "wsc_config"})

(defprocess connection {})

(defprocess web-service-security)


