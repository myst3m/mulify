(ns mulify.secure-properties
  (:gen-class )
  (:require [clojure.data.xml :as dx]
            [silvur.util :refer [uuid]]
            [clojure.string :as str]
            [mulify.core :refer (defprocess >camel)]))

(defprocess config {:name "configuration-properties-secured-env"
                    :file "config/config-secured-${mule.env}.yaml"
                    :key "${mule.key}"})

(defprocess encrypt {:algorithm "Blowfish"})

