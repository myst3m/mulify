(ns mulify.tls
  (:require [mulify.core :refer (defprocess)]))

(defprocess context)

(defprocess key-store {:type "pkcs12" :path "keystore.p12" :keyPassword "password" :password "password"})

(defprocess trust-store {:type "pkcs12"})

