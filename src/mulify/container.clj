(ns mulify.container
  (:import [org.mule.runtime.module.launcher MuleContainer ]
           [org.mule.runtime.module.reboot MuleContainerBootstrap]
           [org.mule.runtime.module.reboot.internal DefaultMuleClassPathConfig]
           [org.mule.runtime.core.api.context DefaultMuleContextFactory]
           [org.mule.runtime.config.internal SpringXmlConfigurationBuilder]
           [org.apache.commons.io.filefilter SuffixFileFilter])
  (:require [mount.core :as mount :refer [defstate]]
            [clojure.java.io :as io]
            [clojure.string :as str]))



(defn start []
  (System/setProperty "mule.home" "/home/myst/projects/mulify")
  (System/setProperty "mule.base" "/home/myst/projects/mulify")

  (doto (MuleContainer. (into-array String []))
    (.start  true)))

(defn stop [cont]
  (.stop cont))

(defstate container :start (start) :stop (stop container))
