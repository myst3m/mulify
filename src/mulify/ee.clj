(ns mulify.ee
  (:require [mulify.doc :as doc]
            [mulify.core :refer (defprocess contains-tag?)]
            [silvur.util :refer [uuid]]
            [clojure.data.xml :as dx]
            [clojure.spec.alpha :as s]
            [clojure.spec.test.alpha :as t]
            [mulify.dataweave :as dw]))

(defprocess transform {:display-name "transorm"} :key-fn identity)
(defprocess message)
(defprocess object-store-caching-strategy)
(defprocess cache)
(defprocess set-payload)
(defprocess set-variable {:variable-name ""} :update-fn dx/cdata)
(defprocess variables)
(defprocess message)

;; (defn transform* [& {:keys [display-name resource dw]  :as params} ]
;;   (transform
;;    {:display-name display-name}
;;    (message
;;     (set-payload (if-not (or dw resource)
;;                    "output json --- payload"
;;                    (or dw (select-keys params [:resource] )))))))


(defn transform* [display-name & payload-and-key-values]
  (let [[kvs payload] (if (odd? (count payload-and-key-values))
                        [(partition-all 2 (rest payload-and-key-values))
                         (first payload-and-key-values)]
                        [(partition-all 2 payload-and-key-values)])]
    (transform {:display-name display-name}
      (message
        (if (or (string? payload) (nil? payload))
          (set-payload (or payload (dw/wrap "payload")))
          (set-payload {:resource (str payload)})))
      (apply variables
                 (reduce (fn [tr [k v]]
                           (conj tr (if (string? v)
                                      (set-variable {:variable-name (name k)} v)
                                      (set-variable {:variable-name (name k) :resource (str v)}))))
                   []
                   kvs)))))


(s/fdef transform
  :args (s/cat
          :attr (s/? (s/keys ::opt-un [::display-name]))
          :items (s/+ (contains-tag? ::message ::variables))))

;; (s/fdef message
;;   :args (s/alt :no-args (s/cat) ;;:payload (s/* (contains-tag? ::set-payload))
;;                ))

(s/fdef variables
  :args (s/cat
          :attr (s/? map?)
          :items (s/* (contains-tag? ::set-variable))))

(s/fdef set-payload
  :args (s/alt 
          :dw string?
          :resource (s/keys :req-un [::resource])))

(s/fdef set-variable
  :args (s/cat
          :attr (s/keys :req-un [::variable-name])
          :variable (s/alt
                     :cdata (partial instance? clojure.data.xml.node.CData)
                     :fx string?)))



(t/instrument)

