(ns mulify.vm
  (:refer-clojure :exclude [read])
  (:require [clojure.data.xml :as dx]
            [clojure.string :as str]
            [mulify.core :refer [defprocess]]
            [mulify.dataweave :as dw]
            [mulify.core :refer (defprocess >camel)]))


(defprocess config {:name "vm_config"})
(defprocess queues)

(defprocess queue {:queue-name "queue_name"})


(comment
  (mulify.vm/config
  {:name "vm_config"}
  (mulify.vm/queues (mulify.vm/queue {:queueName "queue_name"}))))
