(ns mulify.generic
  (:require [mulify.core :refer [defprocess >snake >camel]]
            [clojure.string :as str]
            [clojure.data.xml :as xml]))

(defn- config-name [name-or-map]
  (if (map? name-or-map)
    (or (:name name-or-map) (str (:connector-name name-or-map) "-config" ))
    name-or-map))

(defn connector [{:keys [connector-type config-ref key-fn] :as attrs :or {key-fn identity}} & body]
  (let [[cns cn] (str/split (name connector-type)  #":")
        attrs (update attrs :config-ref  config-name)]

    (apply xml/element
           (keyword (str "mulify." cns) cn)
           (key-fn (>camel (dissoc attrs :connector-type :key-fn)))
           body)))


;;  <account-api:config name="Account_API_Config" doc:name="Account API Config" doc:id="112c8cd9-827a-4e35-b6f2-fbd17c01d278" property_host="host.com" property_port="8080" property_basePath="/basepath" property_protocol="HTTP" property_responseTimeout="3000"/>

(defn config [{:keys [name connector-name
                      key-fn
                      property-host property-port property-base-path property-protocol property-response-timeout]
               :as attrs :or {key-fn identity}} & body]
  (let [cn connector-name
        config-name (or name (str connector-name "-config" ))]
    (apply xml/element
           (keyword (str "mulify." cn) "config")
           (key-fn {:name config-name
                    :doc:name (or (attrs :doc:name) config-name)
                    :property_host (or property-host "localhost")
                    :property_port (or property-port "443")
                    :property_protocol (or property-protocol "HTTPS")
                    :property_basePath (or property-base-path "/")
                    :property_responseTimeout (or property-response-timeout "3000")})
           body)))


