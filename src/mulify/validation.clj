(ns mulify.validation
  (:refer-clojure :exclude [update])
  (:require [clojure.data.xml :as dx]
            [silvur.util :refer [uuid]]
            [clojure.string :as str]
            [mulify.core :refer (defprocess >camel)]))

(defprocess is-number {:number-type "LONG" :value "#[payload]" :message "exception message"})
(defprocess is-true {:expression "#[payload]" :message "exception message"})
(defprocess is-false {:expression "#[payload]" :message "exception message"})
(defprocess is-not-null {:value "#[payload]"})
(defprocess is-not-blank-string {:value "#[payload]"})
(defprocess matches-regex {:value "#[payload]" :regex ""})
(defprocess is-not-empty-collection {:message "not found"})
