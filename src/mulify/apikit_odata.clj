(ns mulify.apikit-odata
  (:require [clojure.data.xml :as dx]
            [clojure.string :as str]
            [clojure.java.io :as io]
            [mulify.core :refer (defprocess global-process-map >camel flow)]
            [mulify.db]
            [mulify.http]))

(defprocess config {:name "odata-metadata-config",
                    :api-definition "api/odata-metadata.csdl.xml"})

(defprocess route {:config-ref "odata-metadata-config"})

(defprocess http-request-parameters
  {:listener-path "#[attributes.listenerPath]"
   :method "#[attributes.method]"
   :scheme "#[upper(attributes.scheme)]"
   :host "#[attributes.headers.'host']"
   :http-headers "#[attributes.headers]"
   :query-string "#[attributes.queryString]"
   :masked-request-path "#[attributes.maskedRequestPath]"})


(defprocess request-entity-listener
  {:config-ref "odata-metadata-config"
   :path "/"
   :method "GET"})

(defprocess transform-to-sql-select
  {:config-ref "odata-metadata-config"
   :entity-set-name "#[attributes.entitySetName]"})

(defprocess request-parameters
  {:entity-type-name
   "#[attributes.odataRequestAttributes.entityTypeName]"
   :entity-type-fields
   "#[attributes.odataRequestAttributes.entityTypeFields]"
   :entity-type-keys
   "#[attributes.odataRequestAttributes.entityTypeKeys]"
   :system-query-options
   "#[attributes.odataRequestAttributes.systemQueryOptions]"})

(defprocess serialize-entity
  {:config-ref "odata-metadata-config"
    :path "/"
   :method "GET"})

(defprocess serialize-entity-collection
  {:config-ref "odata-metadata-config"
    :path "/"
   :method "GET"})

(defprocess inbound-content)

(defprocess request-entity-collection-listener
  {:config-ref "odata-metadata-config",
    :path "/",
    :method "GET"})


(defn transform-to-sql-select*
  ([{:keys [config-ref]
     :or {config-ref "odata-metadata-config"}}]
   (transform-to-sql-select
    {:config-ref config-ref
     :entitySetName "#[attributes.entitySetName]"}
    (request-parameters
     {:entity-type-name
      "#[attributes.odataRequestAttributes.entityTypeName]",
      :entity-type-fields
      "#[attributes.odataRequestAttributes.entityTypeFields]",
      :entity-type-keys
      "#[attributes.odataRequestAttributes.entityTypeKeys]",
      :system-query-options
      "#[attributes.odataRequestAttributes.systemQueryOptions]"})))

  )


(defn odata->select* [{:keys [config-ref]}]
  (transform-to-sql-select
    {:config-ref config-ref,
     :entity-setName "#[attributes.entitySetName]"}
    (request-parameters
     {:entity-type-name
      "#[attributes.odataRequestAttributes.entityTypeName]",
      :entity-type-fields
      "#[attributes.odataRequestAttributes.entityTypeFields]",
      :entity-type-keys
      "#[attributes.odataRequestAttributes.entityTypeKeys]",
      :system-query-options
      "#[attributes.odataRequestAttributes.systemQueryOptions]"})))


(defmacro entity-flow* [{:keys [config-ref path method db-config-ref]
                         :or {db-config-ref "Database_Config"}} & body]
  `(mulify.core/flow
    {:name (str (str/upper-case ~method)  (str/replace ~path #"/" "\\\\") "\\" "ENTITY")}
    (request-entity-listener
     {:config-ref ~config-ref
      :path ~path
      :method (str/upper-case ~method)})
    ~@body
    (serialize-entity
     {:config-ref ~config-ref
      :path ~path
      :method (str/upper-case ~method)}
     (mulify.apikit-odata/inbound-content
      "#[%dw 2.0 output application/json --- payload[0]]"))))


(defmacro entity-collection-flow* [{:keys [config-ref path method db-config-ref]
                                    :or {db-config-ref "Database_Config"}} & body]
  `(mulify.core/flow
    {:name (str (str/upper-case ~method)  (str/replace ~path #"/" "\\\\") "\\" "ENTITY_COLLECTION")}
    (request-entity-collection-listener
     {:config-ref ~config-ref
      :path ~path
      :method (str/upper-case ~method)})
    ~@body
    (serialize-entity-collection
     {:config-ref ~config-ref
      :path ~path
      :method ~method}
     (inbound-content
      "#[%dw 2.0 output application/json --- { \"value\": payload }]"))))



(defmacro main-flow* [{:keys [name config-ref]
                       :or {name "main-odata-flow"
                            config-ref "HTTPS_Listener_Config"}}
                      & body]
  `(mulify.core/flow
    {:name ~name}
    
    ~@body

    (route
     {:config-ref ~config-ref}
     (mulify.apikit-odata/http-request-parameters
      {:listener-path "#[attributes.listenerPath]"
       :method "#[attributes.method]"
       :scheme "#[upper(attributes.scheme)]"
       :host "#[attributes.headers.'host']"
       :http-headers "#[attributes.headers]"
       :query-string "#[attributes.queryString]"
       :maskedRequestPath "#[attributes.maskedRequestPath]"}))
    (mulify.core/error-handler
     {:name "Error_Handler"}
     (mulify.core/on-error-propagate
      {:name "On_Error_Propagate"
       :enable-notifications "true"
       :log-exception "true"
       :type "MULE:ANY"}
      (mulify.ee/transform
       (mulify.ee/message
        (mulify.ee/set-payload
         "%dw 2.0\noutput application/json\n---\n{\n\terror: {\n\t\tcode: error.errorMessage.payload.code default \"UNKNOWN\",\n\t\tmessage: error.errorMessage.payload.message default error.description,\n\t\ttarget: error.errorMessage.payload.target,\n\t\tdetails: error.errorMessage.payload.details default [{code: \"UNKNOWN\", message: error.detailedDescription, target: null}],\n\t\tinnererror: error.errorMessage.payload.innerError default error.childErrors\n\t}\n}"))
       (mulify.ee/variables
        (mulify.ee/set-variable
         {:variable-name "statusCode"}
         "error.errorMessage.payload.statusCode default 500")))))))
