(ns mulify.ibm-mq
  (:refer-clojure :exclude [update binding])
  (:require [clojure.data.xml :as dx]
            [silvur.util :refer [uuid]]
            [clojure.string :as str]
            [clojure.spec.alpha :as s]
            [clojure.spec.test.alpha :as t]
            [mulify.core :refer (defprocess >camel contains-tag?)]))

		;; <dependency>
		;; 	<groupId>com.mulesoft.connectors</groupId>
		;; 	<artifactId>mule-ibm-mq-connector</artifactId>
		;; 	<version>1.6.16</version>
		;; 	<classifier>mule-plugin</classifier>
		;; </dependency>
		;; <dependency>
		;; 	<groupId>com.ibm.mq</groupId>
		;; 	<artifactId>com.ibm.mq.allclient</artifactId>
		;; 	<version>9.3.1.0</version>
		;; </dependency>

;; <sharedLibraries>
;; 						<sharedLibrary>
;; 							<groupId>com.ibm.mq</groupId>
;; 							<artifactId>com.ibm.mq.allclient</artifactId>
;; 						</sharedLibrary>
;; 					</sharedLibraries>

(defprocess config {:name "IBM_MQ_Config"})
(defprocess connection {:username "" :password "" :client-id ""})
(defprocess connection-mode)
(defprocess binding {:queue-manager ""})
(defprocess client {:host "localhost" :queue-manager "QM1" :channel "DEV.APP.SVRCONN"})
(defprocess caching-strategy)
(defprocess no-caching)
(defprocess consumer-type)
(defprocess queue-consumer)


(defprocess listener {:config-ref "IBM_MQ_Config" :destination "" :ack-mode "AUTO"})
(defprocess consume {:config-ref "IBM_MQ_Config" :destination "" :ack-mode "AUTO"})
(defprocess publish {:config-ref "IBM_MQ_Config" :destination ""})
(defprocess publish-consume {:config-ref "IBM_MQ_Config"
                             :destination ""
                             :send-correlation-id "AUTO"})
(defprocess message)
(defprocess body)

;; Config
(s/fdef config
  :args (s/cat
          :attr (s/? (s/keys :req-un [::name]))
          :connection (contains-tag? ::connection)))

(s/fdef connection
  :args (s/cat
          :attr (s/keys :opt-un [::username ::password ::client-id ::ccs-id ])
          :items (s/* (contains-tag? ::connection-mode ::caching-strategy))))

(s/fdef connection-mode
  :args (s/cat
         :attr (s/keys :opt-un [::binding])))

(s/fdef caching-strategy
  :args (s/cat
         :items (s/keys :opt-un [::no-caching])))

(s/fdef binding
  :args (s/cat
          :attr (s/keys :req-un [::queue-manager])))


;; Listener

(s/fdef listener
  :args (s/cat
          :attr (s/keys :req-un [::config-ref ::destination ::ack-mode])))

;; Processors
(s/fdef consume
  :args (s/cat
          :attr (s/keys :req-un [::config-ref ::destination ]
                        :opt-un [::ack-mode ::selector ::maximum-wait ::maximam-wait-unit])
          :item (s/? (contains-tag? ::consumer-type))))

(s/fdef consumer-type
  :args (s/cat
         :item (s/? (contains-tag? ::queue-consumer))))

(s/fdef publish
  :args (s/cat
          :attr (s/keys :req-un [::config-ref ::destination]
                        :opt-un [::ack-mode])
          :items (s/? (contains-tag? ::message))))

;; Message

(s/fdef message
  :args (s/cat :body (contains-tag? ::body)))

(s/fdef body
  :args (s/cat :cdata (partial instance? clojure.data.xml.node.CData)))



(t/instrument)

