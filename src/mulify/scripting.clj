(ns mulify.scripting
  (:require [clojure.data.xml :as dx]
            [silvur.util :refer [uuid]]
            [silvur.log :as log]
            [clojure.string :as str]
            [mulify.core :refer (defprocess >camel)]
            [clojure.spec.alpha :as s]
            [mulify.dataweave :as dw]
            [clojure.spec.test.alpha :as t]
            [mulify.core :as c]))

(defprocess execute {:engine "Groovy"})
(defprocess code)

