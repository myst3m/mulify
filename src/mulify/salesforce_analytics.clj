(ns mulify.salesforce-analytics
  (:require [clojure.data.xml :as dx]
            [silvur.util :refer [uuid]]
            [mulify.core :refer (defprocess >camel)]))

(defprocess salesforce-analytics-config {:name "Salesforce_Analytics_Config"})

(defprocess basic-connection {:username      "username",
                              :password      "password",
                              :securityToken "security-token"})

(defprocess upload-external-data-into-new-data-set-and-start-processing
  {:config-ref  "Salesforce_Analytics_Config",
   :operation   "APPEND",
   :description "testset-desc",
   :label       "testset-label",
   :dataSetName "test-set",
   :type        "meta-data.json"})


