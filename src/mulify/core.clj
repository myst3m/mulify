(ns mulify.core
  (:gen-class )
  (:refer-clojure :exclude [error-handler try import])
  (:require [clojure.tools.cli :refer (parse-opts)]
            [clojure.string :as str]
            [clojure.java.io :as io]
            [clojure.xml :as xml]
            [clojure.zip :as zip]
            [clojure.data.xml :as dx]
            [clojure.spec.alpha :as s]
            [clojure.spec.test.alpha :as t]
            [silvur.datetime]
            [silvur.util :refer [uuid]]
            [taoensso.timbre :as log]
            [camel-snake-kebab.core :as csk]
            [camel-snake-kebab.extras :as cske]
            [clj-yaml.core :as yaml]
            [mulify.dataweave :as dw]
            [mulify.pom :as pom])
  (:import [java.util.zip ZipFile ZipInputStream]))


(defn refer-mulify []
  (require '[mulify
             [utils :as utils]
             [http :as http]
             [pom :as pom :refer [pom-update!]]
             [os :as os]
             [vm :as vm]
             [ee :as ee]
             [db :as db]
             [tls :as tls]
             [dataweave :as dw]
             [batch :as batch]
             [apikit :as ak]
             [apikit-odata :as ak-odata]
             [jms :as jms]
             [wsc :as wsc]
             [anypoint-mq :as amq]
             [api-gateway :as gw]
             [generic]
             [salesforce :as sf]
             [edifact :as edi]
             [secure-properties :as sp]
             [scripting :as sc]
             [file :as f]]))

(def global-configs {})


(def schema-location [["http://www.mulesoft.org/schema/mule/core"
                       "http://www.mulesoft.org/schema/mule/core/current/mule.xsd"]
                      ["http://www.mulesoft.org/schema/mule/file"
                       "http://www.mulesoft.org/schema/mule/file/current/mule-file.xsd"]
                      ["http://www.mulesoft.org/schema/mule/apikit-odata"
                       "http://www.mulesoft.org/schema/mule/apikit-odata/current/mule-apikit-odata.xsd"]
                      ["http://www.mulesoft.org/schema/mule/batch"
                       "http://www.mulesoft.org/schema/mule/batch/current/mule-batch.xsd"]
                      ["http://www.mulesoft.org/schema/mule/http"
                       "http://www.mulesoft.org/schema/mule/http/current/mule-http.xsd"]
                      ["http://www.mulesoft.org/schema/mule/mule-apikit"
                       "http://www.mulesoft.org/schema/mule/mule-apikit/current/mule-apikit.xsd"]
                      ["http://www.mulesoft.org/schema/mule/db"
                       "http://www.mulesoft.org/schema/mule/db/current/mule-db.xsd"]
                      ["http://www.mulesoft.org/schema/mule/mongo"
                       "http://www.mulesoft.org/schema/mule/mongo/current/mule-mongo.xsd"]
                      ["http://www.mulesoft.org/schema/mule/ee/core"
                       "http://www.mulesoft.org/schema/mule/ee/core/current/mule-ee.xsd"]
                      ["http://www.mulesoft.org/schema/mule/salesforce"
                       "http://www.mulesoft.org/schema/mule/salesforce/current/mule-salesforce.xsd"]
                      ["http://www.mulesoft.org/schema/mule/apikit-soap"
                       "http://www.mulesoft.org/schema/mule/apikit-soap/current/mule-apikit-soap.xsd"]
                      ["http://www.mulesoft.org/schema/mule/wsc"
                       "http://www.mulesoft.org/schema/mule/wsc/current/mule-wsc.xsd"]
                      ["http://www.mulesoft.org/schema/mule/google-sheets"
                       "http://www.mulesoft.org/schema/mule/google-sheets/current/mule-google-sheets.xsd"]
                      ["http://www.mulesoft.org/schema/mule/api-gateway"
                       "http://www.mulesoft.org/schema/mule/api-gateway/current/mule-api-gateway.xsd"]
                      
                      ["http://www.mulesoft.org/schema/mule/tls"
                       "http://www.mulesoft.org/schema/mule/tls/current/mule-tls.xsd"]

                      ["http://www.mulesoft.org/schema/mule/anypoint-mq"
                       "http://www.mulesoft.org/schema/mule/anypoint-mq/current/mule-anypoint-mq.xsd"]
                      
                      ["http://www.mulesoft.org/schema/mule/mqtt3"
                       "http://www.mulesoft.org/schema/mule/mqtt3/current/mule-mqtt3.xsd"]
                      ;; Enterprise 
                      ["http://www.mulesoft.org/schema/mule/core"
                       "http://www.mulesoft.org/schema/mule/core/current/mule.xsd"]
                      ;; ["http://www.mulesoft.org/schema/mule/ee/tracking"
                      ;;  "http://www.mulesoft.org/schema/mule/ee/tracking/current/mule-tracking-ee.xsd"]
                      ["http://www.mulesoft.org/schema/mule/validation"
                       "http://www.mulesoft.org/schema/mule/validation/current/mule-validation.xsd"]
                      ])

;;

(def ^:dynamic *mule-home*)

(defn set-mule-home! [path-as-string]
  (alter-var-root #'*mule-home* (constantly (str/replace path-as-string #"^~" (System/getenv "HOME")))))

(defn mule-apps-path [& paths]
  (try
    (str (apply io/file *mule-home* "apps" paths))
    (catch Exception e (log/info "*mule-home* is not configured."))))


(def raw-reserved-keyword #{"config-ref"})

(defn >camel [m]
  (reduce (fn [r [k v]]
            (conj r {(keyword (if (raw-reserved-keyword (name k))
                                (str (csk/->camelCaseString  (str/replace (name k) #"-ref$" "")) "-ref")
                                (csk/->camelCaseKeyword k)))
                     v}))
          {}
          m))

(defn >snake [m]
  (reduce (fn [r [k v]]
            (conj r {(keyword (if (raw-reserved-keyword (name k))
                                (name k)
                                (csk/->snake_case_string  (name k))))
                     v}))
          {}
          m))


;;  {:mulify.core/when mulify.core/+when, ...}
(def global-process-map (atom {}))

;; Maco to define functions
(defmacro defprocess [process-name & default-params-and-key-params]
  (let [[default-params
         {:keys [key key-fn update-fn xml-real-ns merge-params?]
          :or   {key-fn        >camel
                 merge-params? true
                 update-fn     identity}}] (cond
                                       ;; handle attributes given as map
                                             (map? (first default-params-and-key-params))
                                             (let [[xm] default-params-and-key-params]
                                               [(-> (reduce (fn [r x] (assoc r x "_")) {} (:_ xm))
                                                    (conj (dissoc xm :_)))
                                                (into {} (map vec (partition 2 (rest default-params-and-key-params))))])
                                             ;; handle variables 
                                             (symbol? (first default-params-and-key-params))
                                             (do
                                               [(first default-params-and-key-params) (into {} (map vec (partition 2 (rest default-params-and-key-params))))])
                                             :else
                                             (do
                                               [{} (into {} (map vec (partition 2 default-params-and-key-params)))]))

        current-ns    (str (ns-name *ns*))
        qualified-key (or key (keyword (or xml-real-ns current-ns) (str process-name)))
        target-key    (keyword current-ns (str process-name))
        body 'body
        params 'params]
    (swap! global-process-map assoc qualified-key (symbol target-key))
    (do
      (list 'defn process-name
            (list []
                  (list process-name {}))
            (list (vector {:keys (mapv (comp symbol name) (keys default-params)) :as params} '& body)
                  `(let [key#            ~qualified-key
                         key-fn# (comp ~key-fn #(update-keys %
                                                             (fn [k#]
                                                               (if (= :display-name k#)
                                                                 :doc:name
                                                                 k#))))
                         ]
                     (->> (cond
                            (or (:tag ~params) (instance? clojure.data.xml.node.CData ~params))
                            (apply dx/element key#  (key-fn# ~default-params) (cons ~params ~body))

                            (string? ~params)
                            (dx/element key#  (key-fn# ~default-params) (~update-fn ~params))

                            :else
                            (apply dx/element key# (let [user-params# (cond-> ~params
                                                                        (:name ~params) (update :name str/replace #" +" "_"))
                                                         merged-params# (cond
                                                                          (and ~merge-params? (seq user-params#))
                                                                          (merge-with (fn [l# r#] (or r# l#)) (into {} (filter #(not= "_" (str (second %))) ~default-params)) user-params#)
                                                                          (and (not ~merge-params?) (seq user-params#))
                                                                          user-params#
                                                                          :else
                                                                          (merge-with (fn [l# r#] (or r# l#)) (into {} (filter #(not= "_" (str (second %))) ~default-params))  user-params#))]
                                                     
                                                     (key-fn# merged-params#))
                                   (map ~update-fn (flatten ~body)))))))))))

(defprocess configuration-properties {:file "mule-artifact.properties"})

(defprocess configuration {:default-error-handler-ref "default-error-handler"})

(defn gen-mule-namespaces []
  (let [additional-mule-namespaces (keep :connector-name global-configs)]
    (-> {:xmlns:core          "http://www.mulesoft.org/schema/mule/core"
         :xmlns:batch         "http://www.mulesoft.org/schema/mule/batch"
         :xmlns:apikit-soap   "http://www.mulesoft.org/schema/mule/apikit-soap"
         :xmlns:apikit-odata  "http://www.mulesoft.org/schema/mule/mule-apikit-odata"  
         :xmlns:mqtt3         "http://www.mulesoft.org/schema/mule/mqtt3"         
         :xmlns:http          "http://www.mulesoft.org/schema/mule/http"
         :xmlns:db            "http://www.mulesoft.org/schema/mule/db"
         :xmlns:file          "http://www.mulesoft.org/schema/mule/file"
         :xmlns:wsc           "http://www.mulesoft.org/schema/mule/wsc"
         :xmlns:mule-apikit   "http://www.mulesoft.org/schema/mule/mule-apikit"
         :xmlns:anypoint-mq   "http://www.mulesoft.org/schema/mule/anypoint-mq"
         :xmlns:api-gateway   "http://www.mulesoft.org/schema/mule/api-gateway"         
         :xmlns:xsi           "http://www.w3.org/2001/XMLSchema-instance"
         :xmlns:mongo         "http://www.mulesoft.org/schema/mule/mongo"
         :xmlns:doc           "http://www.mulesoft.org/schema/mule/documentation"
         :xmlns:google-sheets "http://www.mulesoft.org/schema/mule/google-sheets"
         :xmlns:salesforce    "http://www.mulesoft.org/schema/mule/salesforce"
         :xmlns:tls           "http://www.mulesoft.org/schema/mule/tls"
         :xmlns:validation    "http://www.mulesoft.org/schema/mule/validation"
         ;; Enterprise
         :xmlns:ee            "http://www.mulesoft.org/schema/mule/ee/core"
         :xmlns:tracking      "http://www.mulesoft.org/schema/mule/ee/tracking"
         ;; XSI
         :xsi:schemaLocation  schema-location}
        (merge (reduce (fn [r n]
                         (assoc r (keyword (str "xmlns:" n))
                                (str "http://www.mulesoft.org/schema/mule/" n)))
                       {}
                       additional-mule-namespaces))
        (update :xsi:schemaLocation (fn [xm]
                                      (->> additional-mule-namespaces
                                           (map #(vector (str "http://www.mulesoft.org/schema/mule/" %)
                                                         (str "http://www.mulesoft.org/schema/mule/" % "/current/mule-" % ".xsd")))
                                           (concat xm))) )
        (update :xsi:schemaLocation #(str/join " " (flatten %))))))

;; http://www.mulesoft.org/schema/mule/account-api http://www.mulesoft.org/schema/mule/account-api/current/mule-account-api.xsd" 

;; If it is not preferred to merge params when users give the params, set  :merge-params? to false.
(defprocess mule  {}  :key-fn identity)

(defprocess flow {:name (str (gensym "Flow_"))})

(defprocess choice {:display-name "choice"})

(defprocess +when {:expression "#[payload.handler.name == \"order\"]"} :key ::when)
(defprocess +otherwise {} :key ::otherwise)

(defprocess logger {:display-name "logger" :level "INFO" :message ""}  :key-fn #(update % :level (comp str/upper-case name)))

(defn logger*
  ([level message]
   (logger {:level level :message message}))
  ([name level message]
   (logger {:documentation:name name :level level :message message})))

(defprocess set-variable {:value "" :variable-name ""})



(defprocess set-payload {:value "#[payload]"})

(defprocess async)

(defprocess error-handler {:name "error-handler"})
(defprocess error-mapping {:source-type "ANY" :target-type ""})

(defprocess on-error-propagate {:type "ANY"})

(defprocess on-error-continue {:type "ANY" :log-exception true :enable-notifications true})

(defprocess on-error {:ref "error-ref-name"})

(defprocess raise-error {:type "MULE:EXPRESSION" :description "error occurred"})

(defprocess foreach {:collection (dw/fx "payload")
                     :batch-size "10"
                     :root-message-variable-name "rootMessageVar"
                     :counter-variable-name "counter"})

(defprocess parallel-foreach {:collection (dw/fx "payload")
                              :max-concurrency 1})

(defprocess flow-ref {:name "flow-pointed-target"})

(defprocess sub-flow)

(defprocess first-successful)

(defprocess round-robin)

(defprocess scatter-gather)

(defprocess +route {} :key ::route)

(defprocess configuration-properties {:file "mule-artifact.properties"})

(defprocess try)

(defprocess scheduler)
(defprocess scheduling-strategy)
(defprocess fixed-frequency {:frequency "60000", :start-delay "10", :time-unit "MILLISECONDS"})

(defprocess global-property)
(defprocess import {:file "global-common.xml"})
(defprocess reconnect {:frequency "5000" :count "1"})
(defprocess reconnection)
(defprocess parse-template {:localtion "forms/form.html"})
(defprocess non-repeatable-stream {})

(defn mule-schema-seq [mule-xsd-key-or-schema-location]
  (let [xs (->> (dx/parse-str (slurp (cond (keyword? mule-xsd-key-or-schema-location)
                                           (str "http://www.mulesoft.org/schema/mule/"
                                                (name mule-xsd-key-or-schema-location)
                                                "/current/mule"
                                                (when-not (= mule-xsd-key-or-schema-location :core)
                                                  (str "-" (name mule-xsd-key-or-schema-location)))
                                                ".xsd")
                                           (and (map? mule-xsd-key-or-schema-location)
                                                (get-in mule-xsd-key-or-schema-location [:attrs :schemaLocation]))
                                           (get-in mule-xsd-key-or-schema-location [:attrs :schemaLocation])
                                           
                                           
                                           :else mule-xsd-key-or-schema-location)) :namespace-aware false)
                (clojure.walk/postwalk (fn [z]
                                         (cond (seq? z)    (filter (complement empty?) z)
                                               (string? z) (str/trim z)
                                               :else       z)))
                (xml-seq))]
    ;; Get recursively the elements of :xsd:include
    (if-let [zs (seq (filter (comp #{:xsd:include} :tag) xs))]
      (apply concat xs (map mule-schema-seq zs))
      xs)))


(def e (memoize mule-schema-seq))

(defn query-by-name [element-name xml-tree-seq]
  (first (filter (comp #{element-name} :name :attrs) xml-tree-seq)))


(defn gen-schema [xml-tree-seq]
  (reduce (fn [r x]
            (let [{{name :name type :type ref :ref} :attrs tag :tag} x]
              (cond
                ;; r {type {
                (and (#{:xsd:element} tag) type) (let []
                                                   (-> r
                                                       (assoc-in [(keyword name) :elements]
                                                                 (->> (query-by-name type xml-tree-seq)
                                                                      :content
                                                                      (mapcat (fn [z]
                                                                                (filter (comp #{:xsd:element} :tag) (xml-seq z))))
                                                                      (mapv :attrs)))
                                                       (assoc-in [(keyword name) :attrs]
                                                                 (->> (query-by-name type xml-tree-seq)
                                                                      :content
                                                                      (mapcat (fn [z]
                                                                                (filter (comp #{:xsd:attribute} :tag) (xml-seq z))))
                                                                      (mapv :attrs)))))
                :else r)))
          {}
          xml-tree-seq))


(defn query [e-name xml-tree-seq]
  (get  (gen-schema xml-tree-seq) e-name))



;; To use spec 
(defn contains-tag? [& tags]
  #(some? ((set tags) (:tag %))))


(defn use-global-configs! [& configs]
  (alter-var-root #'global-configs (constantly configs)))

;;
(defmacro defapi [api-name args-vec & body]
  (let [spec-file (str api-name ".raml")
        opts (conj (->> global-configs
                     (filter #(#{'mulify.apikit :apikit 'apikit} (:class %)))
                     (first))
               {:api-path spec-file})]
    `(def ~api-name
       (fn ~args-vec
         (when (empty? global-configs)
           (throw (ex-info "You should define at least one api-kit config, and call mulify.core/set-global-configs!"
                           {:expression '(def apikit-config {:id "1" :class 'mulify.apikit :port "8081"})})))
         (mulify.core/mule (gen-mule-namespaces)
                           ;; Convert to xml/element
                           (->> global-configs
                                (filter #(not= (:class %) 'mulify.apikit)) ;; apikit is configured in on-apikit-listner
                                (reduce (fn [vs# m#]
                                          (if-let [f# (find-var (symbol (name (:class m#)) "config"))]
                                            (cons (f# m#) vs#)
                                            vs#))
                                        [])
                                (map #(update % :attrs dissoc :class)))
                           (mulify.apikit/on-apikit-listener ~opts
                             ~@body))))))

;; Spec

(s/fdef error-handler
  :args (s/cat
          :attr (s/? (s/keys :req-un [::name]))
          :items (s/* (contains-tag? ::on-error-continue ::on-error-propagate))))

(s/fdef on-error-continue
  :args (s/cat
          :error-type (s/keys :req-un [::type])
          :items (s/* map?)))

(s/fdef on-error-propagate
  :args (s/cat
          :error-type (s/keys :req-un [::type])
          :items (s/* map?)))


;; (:extension-documentation
;;  :configs
;;  :config
;;  :description
;;  :parameters
;;  :parameter
;;  :connections
;;  :connection
;;  :extension
;;  :operations
;;  :operation
;;  :sources
;;  :source
;;  :types
;;  :type)


(defn get-params-from-mule-description-file [path-or-search-word]
  (letfn [(-get-params [xs]
            (update-vals (->> (xml-seq xs)
                              (filter (comp #{:operation :source :config :type} :tag) )
                              (map (juxt (comp  :name :attrs) :content) )
                              (into {}))
                         #(->> (:content (first (filter (comp #{:parameters} :tag) %)))
                               (mapv (comp csk/->kebab-case-keyword :name :attrs) ))))]
    (if (.exists (io/file path-or-search-word))
      (-get-params (xml/parse path-or-search-word))
      
      (when-let [jar-path (->> (file-seq (io/file (System/getenv "HOME") ".m2"))
                               (filter #(re-find (re-pattern (str path-or-search-word ".*jar$")) (str %)) )
                               (sort)
                               (last))]
        (println (str jar-path))
        (with-open [zi (ZipInputStream. (io/input-stream jar-path))]
          (as-> (reduce (fn [r z]
                          (if-let [f (.getNextEntry z)]
                            (when (re-find #".*descriptions.*xml$" (.getName f))
                              (reduced f))
                            (reduced nil)))
                        (repeat zi))
              ze
            (if ze
              (loop [buf (byte-array 1024)
                     s ""]
                (let [size (.read zi buf)]
                  (if (= -1 size )
                    s
                    (recur (byte-array 1024) (str s (String. (byte-array size buf))))))))
            (when ze
              (-get-params (xml/parse (java.io.ByteArrayInputStream. (.getBytes ze)))))))))))


(t/instrument)

