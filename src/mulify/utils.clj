(ns mulify.utils
  (:require [clojure.zip :as zip]
            [clojure.data.xml :as dx]
            [clojure.java.io :as io]
            [clojure.string :as str]
            [camel-snake-kebab.core :as csk]
            [camel-snake-kebab.extras :as cske]
            [clojure.pprint :as pp]
            [silvur.http :as http]
            [babashka.fs :as fs]
            [silvur.util :refer [edn->json json->edn edn->xml]]
            [mulify.core :refer [global-process-map set-mule-home! mule-apps-path]]
            [mulify.pom :refer [pom-update!]]
            [mulify http apikit db ee file wsc validation mqtt3 websocket salesforce salesforce-analytics generic]
            [next.jdbc :as jdbc]
            [next.jdbc.result-set :as rs]
            [reitit.coercion.spec :as spec]
            [reitit
             [core :as r]
             [ring :as ring]
             [openapi :as openapi]]
            [taoensso.timbre :as log]
            [mulify.utils :as utils]))

(def ^:dynamic default-project-root (System/getProperty "user.dir"))

(defn get-project-src
  ([]
   (str  (System/getProperty "user.dir") "/src/main/mule/"))
  ([file]
   (let [file (fs/expand-home (or file (str default-project-root "/src/main/mule/mule-config.xml")))]
     (cond
       (fs/exists? file) (str file)
       (fs/exists? (str default-project-root "/src/main/mule/" file)) (str default-project-root "/src/main/mule/" (fs/file-name file))))))


(def camelize (partial cske/transform-keys csk/->camelCaseKeyword))
(def kebabize (partial cske/transform-keys csk/->kebab-case-keyword))

(defn update-node [z]
  (if-not (instance? clojure.data.xml.node.Element z)
    z
    (-> z
        (update :tag #(if (qualified-keyword? %)
                        (str (str/replace (namespace %) #".*\.(.*)$" "$1") ":" (name %))
                        (name %)))
        (update :tag #(if (.matches % "^core:.*")
                        (str/replace % #"core:" "")
                        %))
        (update :attrs #(clojure.set/rename-keys % {:xmlns:documentation :xmlns:doc
                                                    :documentation:id :doc:id
                                                    :documentation:name :doc:name})))))


(defn tree-edit [zipper ;; matcher
                 editor]
  (loop [loc zipper]
    (if (zip/end? loc)
      (zip/root loc)
      (let [new-loc (zip/edit loc editor)]
        (recur (zip/next new-loc))))))


(defn xml->hiccup [tree & [{:keys [namespace-aware doc]
                            :or {namespace-aware true}
                            :as opts}]]
  (let [xml-tree (if (or (string? tree) (instance? java.net.URL tree))
                   (dx/parse (io/input-stream tree) :namespace-aware namespace-aware)
                   tree)]

    ((fn impl [a-tree {:keys [level attrs?]
                       :or {attrs? true level Integer/MAX_VALUE}
                       :as a-opts}]
       (when (< 0 (inc level))
         (cond
           (and (map? a-tree) (:tag a-tree)) (cond-> [(:tag a-tree)]
                                               (and attrs? (:attrs a-tree) (seq (:attrs a-tree)))
                                               (conj (:attrs a-tree))
                                               
                                               (:content a-tree)
                                               (as-> m
                                                   (apply conj m (impl (:content a-tree)
                                                                       {:level (dec level)
                                                                        :attrs? attrs?}))))
           (and (sequential? a-tree)) (filter (complement empty?) (map #(impl % a-opts) a-tree))
           (string? a-tree) (str/trim a-tree)
           :else a-tree)))
     xml-tree opts)))



(defn iter-zip [root]
  (->> root
       (iterate  zip/next)
       (take-while (complement  zip/end?))))


(defn hiccup->clj [root & {:keys [preserve-doc-namespace use-default-mule-ns?] :as opts}]
  (let [xmlns     (volatile! {})
        ;;reverse-global-map (clojure.set/map-invert @global-process-map)
        root-tree (->>  root
                        ;; Create xmlns map to convert as below
                        ;; 
                        (clojure.walk/postwalk (fn [x]
                                                 (cond
                                                   (and (qualified-keyword? x) (.matches (namespace x) "xmlns.*"))

                                                   ;; :xmlns.http%3A%2F%2Fwww.mulesoft.org%2Fschema%2Fmule%2Fee%2Fcore/transform => :mulify.ee/transform

                                                   (do (vswap! xmlns assoc x 
                                                               (if-let [namespace-suffix (last (re-find #".*mule%2F([\p{Alnum}-_]+).*$" (namespace x)))]
                                                                 (let [fn-symbol (keyword (str "mulify." namespace-suffix) (name x))
                                                                       attr-symbol (keyword (str namespace-suffix ":" (name x)))]
                                                                   ;; Add only functions defined in mulify.* ns
                                                                   (get @global-process-map fn-symbol attr-symbol))
                                                                 (let [namespace-suffix (last (re-find #"%2F([\p{Alnum}-_]+)$" (namespace x)))]
                                                                   (keyword namespace-suffix  (name x)))))
                                                       x)
                                                   
                                                   
                                                   :else x)))
                        
                        (clojure.walk/postwalk (fn [x]
                                                 ;; functions     : Already convered in previous process as mulify.core/mule
                                                 ;; attributes    : :xmlns.http%3A%2F%2Fwww.mulesoft.org%2Fschema%2Fmule%2Fdocumentation/name
                                                 ;; xmlns: {:xmlns.http%3A%2F%2Fwww.mulesoft.org%2Fschema%2Fmule%2Fee%2Fcore/transform :mulify.ee/transform , ..}
                                                 (cond
                                                   (and (qualified-keyword? x) (.matches (namespace x) "xmlns.*"))
                                                   (get @xmlns x x)
                                                   :else x)))
                        (clojure.walk/postwalk (fn [x]
                                                 (if (and (map? x) (not preserve-doc-namespace))
                                                   (->> x
                                                        (filterv (fn [[k v]]
                                                                   (not (.matches (name k) "^doc.*"))
                                                                   true))
                                                        (into {})
                                                        (kebabize))
                                                   x)))
                        (clojure.walk/postwalk (fn [x]
                                                 (cond
                                                   ;; Built-in or provided connectors
                                                   (and (vector? x) (map? (second x)) (empty? (second x)))
                                                   (vec (cons (first x) (subvec x 2)))

                                                   ;; Skip documentation:.*
                                                   (and (vector? x) (re-find #"documentation:.*" (name (first x))))
                                                   x
                                                   ;; Use defined connectors
                                                   (and (vector? x) (keyword? (first x)) (re-find #".*:.*" (name (first x))))
                                                   (do
                                                     (vec (apply list 'mulify.generic/connector
                                                                 (merge (and (map? (second x))
                                                                             (second x))
                                                                        {:connector-type (first x)
                                                                         :key-fn '#(update-keys % name)})
                                                                 (if (map? (second x))
                                                                   (rest (rest x))
                                                                   (rest x)))))

                                                   ;; (do
                                                   ;;   (vector 'mulify.generic/connector
                                                   ;;           (merge {:connector-type (first x)}
                                                   ;;                  (when (map? (second x))
                                                   ;;                    (second x)))
                                                   ;;           (next x)))
                                                   :else
                                                   x)))
                        )]


    ;; xmlns
    ;; {mulify.core/+route :xmlns.http%3A%2F%2Fwww.mulesoft.org%2Fschema%2Fmule%2Fcore/route , ...}
    (-> ((fn dig [root]
           (loop [xs#      root
                  results# '()]
             (cond
               (nil? xs#)    results#
               (vector? xs#) (recur (first xs#) (concat results# (map dig (rest xs#))))
               (map? xs#)    (recur nil xs#)
               (string? xs#) (recur nil xs#)
               :else         (recur nil (cons xs# results#)))))
         root-tree)
        (vec)
        (update-in [1] conj (reduce (fn [r [k v]]
                                      (let [decode-uri (java.net.URLDecoder/decode (namespace k))
                                            xmlns-uri (namespace k)]
                                        (cond
                                          (.matches xmlns-uri ".*XMLSchema-instance.*") r
                                          (.matches xmlns-uri "^xmlns.*%2Fmule%2F.*") (assoc r
                                                                                             (keyword (str/replace  (namespace k) #".*mule%2F([\p{Alnum}-_]+).*$" "xmlns:$1"))
                                                                                             (str/replace decode-uri #"xmlns\." ""))
                                          
                                          :else (assoc r (keyword (last (str/split  xmlns-uri #"%2F")) (name k)) (doto (str/replace decode-uri #"xmlns\." ""))))))
                                    {}
                                    @xmlns))
        (update-in [1] clojure.set/rename-keys {:XMLSchema-instance/schemaLocation :xsi:schemaLocation
                                                :schema-location :xsi:schemaLocation})
        (assoc-in [1 :xmlns:xsi]  "http://www.w3.org/2001/XMLSchema-instance")
        (cond-> use-default-mule-ns? ((fn [xs] (cons (first xs) (drop 2 xs)))))
        (seq))))

(defn xml-find [ks v src]
  (with-open [r (io/input-stream src)]
    (doall (filter #(when (and (map? %) (get-in % (vec (flatten [ks]))))
                      (let [z (get-in % (vec (flatten [ks])))]
                        (.matches z v)))
                   (xml-seq (dx/parse r :namespace-aware false))))))


(defn find-mule-element* [ks v & [src]]
  (xml-find ks v (or src "http://www.mulesoft.org/schema/mule/core/current/mule.xsd")))

(def find-mule-element (memoize find-mule-element*))


;; (defn mule-app-xmls [& [workspace-root]]
;;   (let [workspace-root (or workspace-root "/home/myst/AnypointStudio/studio-workspace1/")]
;;     (->> (file-seq (io/file workspace-root))
;;          (keep #(let [[path app] (re-find #"studio-workspace1/([\w-_]+)/.*\.xml$" (str %))]
;;                   (when app
;;                     (re-find (re-pattern (str ".*/" app "/src/main/mule/" app ".xml$")) path))))
;;          (distinct))))

(defn list-xml [& [dir]]
  (->> (or dir (get-project-src))
       io/file
       file-seq
       (filter #(and (not (.isDirectory %)) (.matches (str %) ".*\\.xml$")))
       (map str)))

(defn load-xml [& [file opts]]
  (let [xml-file (str "src/main/mule/" (str/replace  (System/getProperty "user.dir") #".*/(.*)$" "$1") ".xml")]
    (->  (get-project-src (or file xml-file))
         (xml->hiccup opts)
         (hiccup->clj (merge {:use-default-mule-ns? false} opts)))))


;; Special XML zipper so that CData can be well handled
(defn xml-zipper [root]
  (zip/zipper ;; (complement string?)
   ;; To change NOT to dig CData. Original xml-zip digs CData since CData is like {:content "abc"}
   (fn [node]
     (and (not (string? node) )
          (not (instance? clojure.data.xml.node.CData node))))
   
   (comp seq :content)
   (fn [node children]
     (assoc node :content (and children (apply vector children))))
   root))

(defn project-root []
  (let [path (io/file (System/getProperty "user.dir"))]
    (if-let [x (first (filter #(.endsWith (str %) "deps.edn") (.listFiles path)))]
      (.getParent x))))


(defn -transpile [app app-title  & outputs]
  (if (project-root)
    (binding [default-project-root (project-root)]
      (let [{:keys [artifact version deploy?]} (meta app)
            xml-tree-body app
            outs (->> (or outputs [*out*])
                   (map #(cond
                           ;; Return writer
                           (= (class *out*) (class %)) %
                           ;; Return Path string for the relative path to add path prefix
                           (re-find #"^[^/]" %)  (io/file default-project-root %)
                           ;; Return Writer for the absolute path
                           :else (io/writer %)))
                   (reduce (fn [r x]
                             (if (instance? java.io.File x)
                               (if (and deploy? artifact version (mule-apps-path))
                                 (->> r
                                   (cons (io/writer (io/file
                                                      (mule-apps-path)
                                                      app-title
                                                      (.getName (io/file x)))))
                                   (cons (io/writer x)))
                                 (cons (io/writer x) r))
                               (cons x r)))
                     '()))]
        (doseq [o outs]
          (let [wrt o]
            (.write wrt
              (-> (xml-zipper xml-tree-body)
                (tree-edit  update-node)
                (dx/indent-str)
                
                ;; Since xml generator adds name automatically (ex. xmlns => xmlns:g),
                ;; I should modify after generating xml string.
                ;; See also utils/update-node, it should be modified to fix the Mule tools
                (str/replace #"xmlns:core" "xmlns")))
            (.flush wrt)
            (when-not (= (class *out*) (class o)) (.close wrt))))))
    (log/warn "You should configure project root by set-default-project-root!")))


(def sample-flow 
  (with-meta
    (mulify.core/mule
     {:xmlns:http "http://www.mulesoft.org/schema/mule/http",
      :xmlns:documentation
      "http://www.mulesoft.org/schema/mule/documentation",
      :xmlns:ee "http://www.mulesoft.org/schema/mule/ee/core",
      :xmlns:core "http://www.mulesoft.org/schema/mule/core",
      :xsi:schemaLocation
      "http://www.mulesoft.org/schema/mule/core http://www.mulesoft.org/schema/mule/core/current/mule.xsd http://www.mulesoft.org/schema/mule/http http://www.mulesoft.org/schema/mule/http/current/mule-http.xsd http://www.mulesoft.org/schema/mule/ee/core http://www.mulesoft.org/schema/mule/ee/core/current/mule-ee.xsd",
      :xmlns:xsi "http://www.w3.org/2001/XMLSchema-instance"}
     (mulify.http/listener-config
       {:name "HTTP_Listener_config",
        :documentation:name "HTTP Listener config",
        :documentation:id "afc20f7b-d5ee-4a2e-a623-e0a2c2c7d74f"}
       (mulify.http/listener-connection {:host "0.0.0.0", :port "8081"}))

     (mulify.core/flow
       {:name "trainingFlow",
        :documentation:id "130116f1-6b5c-41b7-9789-d40f0b621e2b"}
       (mulify.http/listener
         {:documentation:name "Listener",
          :documentation:id "23fb85f3-a068-4535-b732-75c23d7bdf83",
          :config-ref "HTTP_Listener_config",
          :path "/"})
       (mulify.ee/transform
         {:documentation:name "Transform Message",
          :documentation:id "200dd30f-61a1-47dc-a192-bf6ff44d614e"}
         (mulify.ee/message
           (mulify.ee/set-payload
             "%dw 2.0\noutput application/java\n---\n{\n}")))
       (mulify.core/logger
         {:level "INFO",
          :documentation:name "Logger",
          :documentation:id "7b344035-c503-4b6d-aa94-c14ce9877daa"})))
    {:group "pse" :artifact "mysql-app" :version "0.0.3" :deploy? true}))


(defn get-node-in [zip-tree path]
  (loop [z zip-tree
         p path]
    (cond
      (nil? z) z
      (empty? p) (zip/node (zip/prev z))
      :else  (do
               (if (= (first p) (:tag (zip/node z)))
                 (recur (zip/down z) (rest p))
                 (recur (zip/right z) p)
                 )))))

(defn assoc-node-in [zip-tree path v]
  (loop [z zip-tree
         p path]
    (cond
      (nil? z) z
      (empty? p) (zip/root (zip/append-child (zip/prev z) v))
      :else  (do
               (if (= (first p) (:tag (zip/node z)))
                 (recur (zip/down z) (rest p))
                 (recur (zip/right z) p)
                 )))))

(defn xmls->cljs []
  (dorun
   (map (fn [xml-path]
          (let [user-dir (System/getProperty "user.dir")
                project-name (str/replace (str *ns*) #"(.*)\..*$" "$1")
                xml-file-name (.getName (io/file xml-path))
                base-name (-> xml-file-name
                              (str/replace  #".xml$" "")
                              (str/replace #"-" "_"))
                clj-file (io/file user-dir "src" (str/replace project-name #"-" "_") (str base-name ".clj"))]
            (io/make-parents clj-file)
            (spit clj-file
              (with-out-str
                (clojure.pprint/pprint (concat
                                         (list (symbol (str "ns " project-name "." base-name)))
                                         '((:gen-class )
                                           (:refer-clojure :exclude [error-handler])
                                           (:require [clojure.string :as str]
                                                     [clojure.java.io :as io]
                                                     [clojure.tools.cli :refer (parse-opts)]
                                                     [silvur.datetime :refer (datetime datetime*)]
                                                     [silvur.util :refer (json->edn edn->json)]
                                                     [mulify
                                                      [core :as c :refer [defapi]]
                                                      [http :as http]
                                                      [os :as os]
                                                      [vm :as vm]
                                                      [ee :as ee]
                                                      [db :as db]
                                                      [tls :as tls]
                                                      [dataweave :as dw]
                                                      [batch :as batch]
                                                      [apikit :as ak]
                                                      [apikit :as ak-odata]
                                                      [jms :as jms]
                                                      [wsc :as wsc]
                                                      [anypoint-mq :as amq]
                                                      [api-gateway :as gw]
                                                      [edifact :as edi]
                                                      [generic]
                                                      [file :as f]]
                                                     [mulify.utils :as utils]))))
                (println)
                (println)
                (clojure.pprint/pprint (list (symbol "def api-set") (load-xml xml-path)))
                (println)
                (clojure.pprint/pprint (list 'mulify.core/set-mule-home! "~/srv/mule-enterprise-standalone-4.4.0"))
                (println (list 'comment
                               (list 'utils/transpile 'api-set
                                     :f (pr-str xml-file-name)
                                     :a (pr-str project-name)
                                     :v (pr-str "1.0.0-SNAPSHOT")
                                     :deploy? false)))
                println))))
     (list-xml))))


(defn transpile [api & {:keys [g a v deploy? pom? clean? base-path f deployed-app-name main?]
                      :or {base-path "src/main/mule"
                           main? false
                           pom? false
                           clean? false}}]
  (io/make-parents (io/file base-path "."))
  (when clean?
    (doseq [f (filter #(.isFile %) (file-seq (io/file base-path)))]
      (io/delete-file f)))

  (cond-> (with-meta (if (fn? api) (api) api)
            {:group (or g "io.theorems")
             :artifact (or a "sample")
             :version (or v "0.0.1")
             :deploy? (or deploy? false)})
    pom? (pom-update!)
    (and main? (every? nil? [a v deployed-app-name])) (do (throw (ex-info "Need to set :a or :v or :deployed-app-name" {}) ))
    true (-transpile (or deployed-app-name (str a "-" v "-mule-application"))
                     (if f
                       (str (io/file base-path f))
                       *out*))))

(def sample-spec {:dbtype "mysql" :user "user" :password "password" :dbname "x1"})
(def sql-type->api-type {:VARCHAR string?
                         :INT number?
                         :SMALLINT number?})

(defn get-meta-data [{:keys [dbtype dbname user password] :as spec}]
  (-> (jdbc/get-connection spec)
      (.getMetaData)))

(defn get-tables [meta-data]
  (-> meta-data
      (.getTables nil nil nil (into-array ["TABLE"]))
      (rs/datafiable-result-set )
      (as-> coll (cske/transform-keys csk/->kebab-case-keyword coll))))

(defn get-columns [meta-data table]
  (-> meta-data
      (.getColumns nil nil table nil)
      (rs/datafiable-result-set)
      (as-> coll (cske/transform-keys csk/->kebab-case-keyword coll))))

(defn get-all-meta-data [& [spec]]
  (-> (get-meta-data {:dbtype "mysql" :user "mule" :password "mule123" :dbname "k1"})
      (as-> md
          (->> (get-tables md)
               (mapv #(vector (csk/->kebab-case-keyword (:table-name %))
                              {:table %
                               :columns (get-columns md (:table-name %))}))
               (into {})))))

(defn edn->dwl [m & {:keys [expression]}]
  
  )


;; (defn table->oas-spec [db-spec]
;;   (-> (get-meta-data db-spec)
;;       (as-> md
;;           (->> (get-tables md)
;;                (mapcat (fn [{:keys [table-name]}]
;;                       [[(str "/" table-name)
;;                         {:get {:summary (str "Get " table-name)
;;                                :responses {200 {:body (->> (map (juxt :column-name :type-name)
;;                                                             (get-columns md "UC10"))
;;                                                        (map #(vector (csk/->kebab-case-keyword (first %)) (sql-type->api-type (keyword (last %)))))
;;                                                        (into {}))}}
;;                                :handler (fn [req] {:status 200})}
;;                         :post {:summary "Create account"
;;                                :parameters {:body (->> (map (juxt :column-name :type-name)
;;                                                             (get-columns md "UC10"))
;;                                                        (map #(vector (csk/->kebab-case-keyword (first %)) (sql-type->api-type (keyword (last %)))))
;;                                                        (into {}))}
;;                                :responses {201 {:body {:id string?}}}
;;                                :handler (fn [req] {:status 201})}}]]))
;;                vec
;;                (reduce conj [["/openapi.json" {:handler (openapi/create-openapi-handler)
;;                                                  :no-doc true}]])
;;                ;;(ring/router)
;;                ;;(ring/ring-handler)
;;                )))

;;   ;; [["/openapi.json" {:handler (openapi/create-openapi-handler)
;;   ;;                    :no-doc true}]
;;   ;;  ["/api"
;;   ;;   ["/accounts/{id}"
;;   ;;    {:get {:summary "Get account"
;;   ;;           :parameters {:path {:id string?}}
;;   ;;           :responses {200 {:body {:id string?}}}
;;   ;;           :handler (fn [req] {:status 200})}
;;   ;;     :post {:summary "Create account"
;;   ;;            :parameters {:body {:type string?}}
;;   ;;            :responses {201 {:body {:id string?
;;   ;;                                    :type string?}}}
;;   ;;            :handler (fn [req] {:status 200})}}]]]
;;   )
