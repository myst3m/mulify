(ns mulify.anypoint-mq
  (:refer-clojure :exclude [update])
  (:require [clojure.data.xml :as dx]
            [silvur.util :refer [uuid]]
            [clojure.string :as str]
            [mulify.core :refer (defprocess >camel)]))


(defprocess config {:name "Anypoint_MQ_Config"})
(defprocess connection {:url "https://mq-ap-northeast-1.anypoint.mulesoft.com/api/v1",
                        :client-id "client-id",
                        :client-secret "client-secret"})

(defn anypoint-mq-config* [{:keys [name url client-id client-secret]}]
  (config
   {:name name}
   (connection
    {:url url
     :client-id client-id
     :client-secret client-secret})))

(defprocess publish {:config-ref "Anypoint_MQ_Config"
                     :destination "queue-name"})

(defn with-anypoint-mq [{:keys [name url client-id client-secret config-prefix] :as opts} & body]
  (if (and client-id client-secret)
    (cons (anypoint-mq-config* (if config-prefix
                                     {:url (str "${" config-prefix ".url}")
                                      :client-id (str "${" config-prefix ".client-id}")
                                      :client-secret (str "${" config-prefix ".client-id}")}
                                     {:url url
                                      :client-id client-id
                                      :client-secret client-secret}))
          body)
    (throw (ex-info "client-id and client-secret required" {}))))


