(ns mulify.batch
  (:require [clojure.string :as str]
            [clojure.java.io :as io]
            [clojure.xml :as xml]
            [clojure.zip :as zip]
            [clojure.data.xml :as dx]
            [silvur.datetime]
            [silvur.util :refer [uuid]]
            [taoensso.timbre :as log]
            [mulify.core :refer (defprocess >camel)]
            [clojure.spec.alpha :as s]
            [clojure.spec.test.alpha :as t]
            [clojure.spec.gen.alpha :as gen]))

(s/def ::job-name (s/with-gen string?
                    (fn [] (gen/such-that #(not= % "")
                                               (gen/string-alphanumeric)))))


(s/fdef job
  :args (s/cat
          :attr (s/? (s/keys :req-un [::job-name]))
          :items (s/* #(-> % :tag #{::process-records ::on-complete}))))

(s/fdef process-records
  :args (s/* #(-> % :tag #{::step})))


(s/fdef step
  :args (s/cat
          :attr (s/? (s/keys :opt-un [::name]))
          :items (s/cat
                   :processors (s/* (partial instance? clojure.data.xml.node.Element))
                   :aggregator (s/? #(-> % :tag #{::aggregator})))))


(s/fdef on-complete )

;;  :block-size "100"
(defprocess job {:job-name "batch_job" :max-failed-records "0" :max-concurrency "1" })
(defprocess process-records {})
(defprocess on-complete {})
(defprocess step {:name "batch_step"})

(defprocess aggregator {:doc:name "batch_aggregator" :preserve-mime-types true :streaming false})
;; aggregator has size option, but cannot set withstreaming = true 

(defn job* [n]
  (let [s1 (gensym "step-")
        s2 (gensym "step-")]
    (job {:job-name n}
      (process-records
        (step {:name s1}
          (mulify.core/logger* :INFO s1)
          (aggregator))
        (step {:name s2}
          (mulify.core/logger* :INFO s2)))
      (on-complete))))

(t/instrument)
