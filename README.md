# 

This software is for your better life.

## Install

## Usage
### CA
Run the following function. It makes a 'ca' directory and ca-key.pem and ca-cert.pem in that directory.
```clj
(build-ca)
```
The configuration file named 'ca.cnf' uses CA_DIR environment variable as the root directory.


## PEM to PKCS
```sh
openssl pkcs12 -export -passout pass:your_password \
  -in /etc/letsencrypt/live/theorems.cc/fullchain.pem \
  -inkey /etc/letsencrypt/live/theorems.cc/privkey.pem \
  -out fullchain_and_key.p12 -name play 
```


```sh
keytool -importkeystore \
  -deststorepass your_password \
  -destkeypass your_password \
  -destkeystore keystore.jks \
  -srckeystore fullchain_and_key.p12 \
  -srcstoretype PKCS12 \
  -srcstorepass your_password \
  -alias play
  ```

